<?php
/**
 * Ce fichier contient les fonctions de service nécessitées par l'utilisation du plugin Cache Factory.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Renvoie la configuration spécifique des caches du plugin.
 *
 * @param string $plugin Préfixe du plugin, à savoir, `territoires_data`.
 *
 * @return array<string, mixed> Tableau de la configuration brute du plugin Territoires
 */
function territoires_data_cache_configurer(string $plugin) : array {
	// Initialisation du tableau de configuration avec les valeurs par défaut du plugin Cache.
	// -- un feed est identifié par son plugin fournisseur et son id.
	//    On utilise le sous-dossier pour le plugin et le premier composant obligatoire du nom pour l'id, `id_feed`
	// -- on distingue 3 types de statistiques, composant obligatoire `type` :
	//    - stats : les indicateurs de statistique descriptive
	//    - clino : les éléments de construction d'une courbe clinographique
	//    - discr : les diverses
	$configuration = [
		'analyse' => [
			'racine'          => '_DIR_VAR',
			'sous_dossier'    => true,
			'nom_obligatoire' => ['id_feed', 'type'],
			'nom_facultatif'  => [],
			'extension'       => '.json',
			'securisation'    => false,
			'serialisation'   => false,
			'codage'          => 'json',
			'separateur'      => '-',
			'conservation'    => 0
		],
	];

	return $configuration;
}
