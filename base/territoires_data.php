<?php
/**
 * Déclarations relatives à la base de données.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Declaration des tables principales.
 *
 * Le plugin Jeux de données pour Territoires rajoute un champ JSON pour stocker la liste des discretisations
 * enregistrées pour chaque feed.
 *
 * @pipeline declarer_tables_principales
 *
 * @param array $tables Description des tables principales de la base de données
 *
 * @return array Description mise à jour avec les nouvelles déclarations
 */
function territoires_data_declarer_tables_principales($tables) {
	$tables['spip_feeds']['field']['discretisations'] = "text DEFAULT '' NOT NULL";
	return $tables;
}

/**
 * Déclaration des tables secondaires (liaisons).
 *
 * Le plugin Jeux de données pour Territoires rajoute l'identifiant du feed dans la table des caractéristiques
 * complémentaires, `spip_territoires_extras`.
 *
 * @pipeline declarer_tables_auxiliaires
 *
 * @param array $tables Description des tables auxilliaires
 *
 * @return array Description complétée des tables auxilliaires
 */
function territoires_data_declarer_tables_auxiliaires(array $tables) : array {
	// Ajout de champs dans la tables des extras de territoires : spip_territoires_extras
	$tables['spip_territoires_extras']['field']['feed_id'] = "varchar(255) DEFAULT '' NOT NULL";

	return $tables;
}

/**
 * Déclaration des informations tierces (alias, traitements, jointures, etc)
 * sur les tables de la base de données modifiées ou ajoutées par le plugin.
 *
 * Le plugin se contente de déclarer le décodage JSON du champ des discrétisation.
 *
 * @pipeline declarer_tables_interfaces
 *
 * @param array $interfaces Tableau global des informations tierces sur les tables de la base de données
 *
 * @return array Tableau fourni en entrée et mis à jour avec les nouvelles informations
 */
function territoires_data_declarer_tables_interfaces(array $interfaces) : array {
	// Les traitements
	// - table spip_feeds : on desérialise les tableaux JSON
	$interfaces['table_des_traitements']['DISCRETISATIONS']['feeds'] = 'json_decode(%s, true)';

	return $interfaces;
}
