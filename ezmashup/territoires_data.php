<?php
/**
 * Ce fichier contient les fonctions de service de Mashup Factory personnalisées par le plugin Statistiques des Territoires.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// -----------------------------------------------------------------------
// -------------------- GESTION DES AUTORISATIONS ------------------------
// -----------------------------------------------------------------------

/**
 * Autorise les actions de création d'un feed éditable.
 *
 * @param null|array|int $qui  L'initiateur de l'action:
 *                             - si null on prend alors visiteur_session
 *                             - un id_auteur (on regarde dans la base)
 *                             - un tableau auteur complet, y compris [restreint]
 * @param array          $feed Description du feed concerné par l'action ou vide si inutile
 *
 * @return bool toujours à `true`
 */
function territoires_data_plugin_autoriser_creer($qui, array $feed) : bool {
	return true;
}

/**
 * Autorise les actions d'édition ou de suppression d'un feed éditable.
 *
 * @param null|array|int $qui  L'initiateur de l'action:
 *                             - si null on prend alors visiteur_session
 *                             - un id_auteur (on regarde dans la base)
 *                             - un tableau auteur complet, y compris [restreint]
 * @param array          $feed Description du feed concerné par l'action ou vide si inutile
 *
 * @return bool toujours à `true`
 */
function territoires_data_plugin_autoriser_modifier($qui, array $feed) : bool {
	return true;
}

/**
 * Autorise les actions d'exécution - peupler et vider - d'un feed.
 * L'unité de traitement à laquelle le feed est associé doit être chargé.
 *
 * @param null|array|int $qui  L'initiateur de l'action:
 *                             - si null on prend alors visiteur_session
 *                             - un id_auteur (on regarde dans la base)
 *                             - un tableau auteur complet, y compris [restreint]
 * @param array          $feed Description du feed concerné par l'action ou vide si inutile
 *
 * @return bool toujours à `true`
 */
function territoires_data_plugin_autoriser_executer($qui, array $feed) : bool {
	// Retrouver les éléments de l'unite de peuplement concernée
	$type = $feed['tags']['type'] ?? '';
	$pays = $feed['tags']['pays'] ?? '';

	// Vérification du chargement de l'unité de peuplement
	include_spip('inc/unite_peuplement');
	return unite_peuplement_est_chargee('territoires', $type, $pays);
}

// -----------------------------------------------------------------------
// ------------------------- FEEDS (services) ----------------------------
// -----------------------------------------------------------------------

/**
 * Renvoie la configuration du dossier relatif où trouver les feeds.
 *
 * Le plugin utilise un dossier spécifique de façon à pouvoir facilement ajouter des feeds sans les mélanger avec
 * ceux des autres plugins utilisateur.
 *
 * @return string Chemin relatif du dossier où chercher les feeds.
 */
function territoires_data_feed_initialiser_dossier() : string {
	return 'territoire_feeds/';
}

/**
 * Renvoie la liste des catégories et leur description.
 *
 * @return array Liste des catégories et de leur description au format [id] = tableau de description avec le nom (label),
 *               la description et l'icone.
 */
function territoires_data_feed_categorie_lister() : array {
	// Initialisation des catégories par défaut
	return [
		'territory_stat' => [
			'name'        => '<:territoires_data:label_feed_category_territory_stat:>',
			'description' => '<:territoires_data:description_feed_category_territory_stat:>',
			'icon'        => 'territory-24.svg'
		],
		'territory_info' => [
			'name'        => '<:territoires_data:label_feed_category_territory_info:>',
			'description' => '<:territoires_data:description_feed_category_territory_info:>',
			'icon'        => 'territory-24.svg'
		],
	];
}

/**
 * Finalise l'exécution d'un feed en mettant à jour la consignation des données extra correspondantes et, pour les feeds
 * de type statistiques (`stat`) crée un cache des statistiques du feed.
 *
 * @uses unite_peuplement_consigne_identifier()
 * @uses territoire_feed_compiler_statistiques()
 *
 * @param array $feed Configuration du feed
 *
 * @return void
 *
 * @throws Exception
 */
function territoires_data_feed_completer_peuplement(array $feed) : void {
	// Retrouver les éléments de l'unite de peuplement concernée
	$type = $feed['tags']['type'] ?? '';
	$pays = $feed['tags']['pays'] ?? '';
	// Retrouver le type d'extra concerné et son abréviation
	$type_extra = $feed['tags']['_type_extra'] ?? '';
	$abr_type_extra = substr($type_extra, 0, -1);

	// Identification de la meta et de la variable de consigne
	include_spip('inc/unite_peuplement');
	$id_consigne = unite_peuplement_consigne_identifier('territoires', $type, $pays);
	if (
		include_spip('inc/config')
		and ($consigne = lire_config($id_consigne, []))
	) {
		// Ajout de la consignation du feed de données en fonction du contexte de la consigne
		if (!in_array($type_extra, $consigne['ext'])) {
			$consigne['ext'][] = $type_extra;
		}
		if (
			empty($consigne[$abr_type_extra])
			or !in_array($feed['feed_id'], $consigne[$abr_type_extra])
		) {
			$consigne[$abr_type_extra][] = $feed['feed_id'];
		}

		// On met à jour la meta
		ecrire_config($id_consigne, $consigne);
	}

	// Création du cache des diverses statistiques si le feed est du type `stat`.
	if ($feed['tags']['_type_extra'] === 'stat') {
		include_spip('territoires_data_fonctions');
		territoire_feed_compiler_statistiques($feed['plugin'], $feed['feed_id']);
		territoire_feed_calculer_geary($feed['plugin'], $feed['feed_id']);
		territoire_feed_aider_discretisation($feed['plugin'], $feed['feed_id']);
		territoire_feed_calculer_courbe_clinographique($feed['plugin'], $feed['feed_id']);
	}
}

/**
 * Finalise le vidage d'un feed en mettant à jour la consignation des données extra correspondantes et, pour les feeds
 *  de type statistiques (`stat`) supprime le cache des statistiques du feed si il existe.
 *
 * @uses unite_peuplement_consigne_identifier()
 *
 * @param array $feed Configuration du feed
 *
 * @return void
 */
function territoires_data_feed_completer_vidage(array $feed) : void {
	// Retrouver les éléments de l'unite de peuplement concernée
	$type = $feed['tags']['type'] ?? '';
	$pays = $feed['tags']['pays'] ?? '';
	// Retrouver le type d'extra concerné et son abréviation
	$type_extra = $feed['tags']['_type_extra'] ?? '';
	$abr_type_extra = substr($type_extra, 0, -1);

	// Identification de la meta et de la variable de consigne
	include_spip('inc/unite_peuplement');
	$id_consigne = unite_peuplement_consigne_identifier('territoires', $type, $pays);
	if (
		include_spip('inc/config')
		and ($consigne = lire_config($id_consigne, []))
		and isset($consigne[$abr_type_extra])
		and in_array($feed['feed_id'], $consigne[$abr_type_extra])
	) {
		// Retrait de la consignation du feed de données statistiques
		$index = array_search($feed['feed_id'], $consigne[$abr_type_extra]);
		if ($index !== false) {
			unset($consigne[$abr_type_extra][$index]);
		}

		// Si la liste des feeds est vide on supprime l'index 'sta' et on retire le type d'extra 'stat' de la liste
		if (!$consigne[$abr_type_extra]) {
			unset($consigne[$abr_type_extra]);
			$index = array_search($type_extra, $consigne['ext']);
			if ($index !== false) {
				unset($consigne['ext'][$index]);
			}
		}

		// On met à jour la meta
		ecrire_config($id_consigne, $consigne);
	}

	// Suppression des caches si le feed est du type `stat`.
	if ($feed['tags']['_type_extra'] === 'stat') {
		include_spip('inc/ezcache_cache');
		$filtres = [
			'sous_dossier' => $feed['plugin'],
			'id_feed'      => $feed['feed_id'],
		];
		$caches = cache_repertorier($feed['plugin'], 'analyse', $filtres);
		cache_vider($feed['plugin'], 'analyse', array_keys($caches), false);
	}
}

/**
 * Renvoie la page de redirection suite à une action d'administration sur un feed.
 * Par défaut, les actions d'administration gérées par Mashup Factory sont `exécuter`, `vider`, `créer`, `éditer` ou `supprimer`.
 * Mais seules les actions de création et d'édition de feed nécessitent une redirection vers le formulaire idoine.
 * Par contre, le plugin Analyse statistique des Territoires, qui est un addon du présent plugin, ajoute une action `analyser`
 * qu'il convient de traiter dans ce service.
 *
 * @param string $action     Action d'aministration venant d'être exécutée avec succès (`creer` ou `editer`)
 * @param array  $feed       Description complète du feed
 * @param string $page_admin URL de la page d'admin d'où provient l'action.
 *
 * @return string URL de redirection
 */
function territoires_data_feed_action_definir_url(string $action, array $feed, string $page_admin) : string {
	// Pas de redirection si l'action n'est pas l'édition ou la création du feed
	$url = '';

	if ($action === 'editer') {
		// Pour une édition, étant donné que l'on a la description du feed, on dispose toujours de toutes les informations
		// pour envoyer vers le formulaire idoine
		$url = parametre_url(
			parametre_url(
				generer_url_ecrire(
					'territoire_feed_editer'
				),
				'feed_id',
				$feed['feed_id']
			),
			'category',
			$feed['category']
		);
	} elseif ($action === 'creer') {
		// Pour une création, suivant que l'on vient de la liste des feeds ou du formulaire d'admin des feeds, l'url ne
		// possèdera pas les mêmes paramètres.
		$url = generer_url_ecrire('territoire_feed_creer');

		// Si on vient du formulaire d'admin des feeds, on rajoute toujours la categorie qui est disponible
		// en paramètre de l'url courante ou de celle de la page d'admin.
		$categorie = _request('category') ?? '';
		if (!$categorie) {
			// On cherche dans l'url de la page d'admin
			parse_str(htmlspecialchars_decode($page_admin), $parametres);
			if (isset($parametres['category'])) {
				$categorie = $parametres['category'];
			}
		}
		if ($categorie) {
			$url = parametre_url($url, 'c', $categorie);
		}
	} elseif ($action === 'analyser') {
		// Pour une analyse statistique, on renvoie vers une page d'affichage du feed et de ces caractéristiques
		// techniques et statistiques.
		$url = parametre_url(
			generer_url_ecrire('territoire_feed'),
			'feed_id',
			$feed['feed_id']
		);

	}

	if ($url) {
		// On redirige toujours vers la page d'admin ayant initiée l'action
		$url = parametre_url($url, 'redirect', $page_admin);
	}

	return $url;
}

/**
 * Complète la liste des actions sur les feeds.
 *
 * Le plugin ajoute l'action `analyser` sur les feeds statistiques (catégorie `category_stat`).
 *
 * @pipeline feed_action_completer_liste
 *
 * @param array $flux Données du pipeline
 *
 * @return array Données du pipeline telles que reçues.
 */
function territoires_data_feed_action_completer_liste(array $flux) : array {
	if ($flux['args']['plugin'] === 'territoires_data') {
		$actions = [
			'analyser' => [
				'name' => '<:territoires_data:bouton_territoire_feed_analyser:>',
				'icon' => '',
				'type' => 'redirect',
				'auth' => 'analyser',
				'feed' => true,
				'menu' => 'actionner',
				'bord' => true
			],
		];
		$flux['data'] = array_merge($flux['data'], $actions);
	}

	return $flux;
}

// -----------------------------------------------------------------------
// ------------------------- SOURCE DATASETS -----------------------------
// -----------------------------------------------------------------------

/**
 * Complète un item de la source venant d'être extrait.
 *
 * Le plugin gère les cas où le code identifiant les territoires n'est pas le code ISO comme l'attend la
 * table spip_territoires_extras (par exemple, le code INSEE en France).
 *
 * @param array $item Item d'un dataset source
 * @param array $feed Configuration du feed
 *
 * @return array Item mis à jour
 */
function territoires_data_item_completer(array $item, array $feed) : array {
	static $type_id = null;
	static $type = null;
	static $pays = null;
	static $codes_to_iso = [];

	// Détermination du tag type_id qui indique quel est le code utilisé pour identifier les territoires et des autres
	// information sur les territoires
	if (null === $type_id) {
		$type_id = $feed['tags']['_type_id'] ?? '';
	}
	if (null === $type) {
		$type = $feed['tags']['type'] ?? '';
	}
	if (null === $pays) {
		$pays = $feed['tags']['pays'] ?? '';
	}

	// On exclut les items mal formés ou dont la valeur est vide
	if (
		$type_id
		and $type
		and (null !== table_valeur($item, $feed['mapping']['basic_fields']['valeur'], null))
	) {
		if ($type_id !== 'iso_territoire') {
			// Si le code utilisé n'est pas le code primaire du territoire, on cherche celui-ci, sinon on renvoie l'item tel que.
			if (!$codes_to_iso) {
				// On recherche la liste des correspondances du code alternatif vers le code iso en une seule fois
				// dans le même hit
				// Quelque soit la demande, on récupère toutes les informations du territoire.
				$select = ['iso_territoire', 'valeur'];
				$where = [
					'type_extra=' . sql_quote('code'),
					'extra=' . sql_quote($type_id),
					'type=' . sql_quote($type),
				];
				if ($pays) {
					$where[] = 'iso_pays=' . sql_quote($pays);
				}
				$extras = sql_allfetsel($select, 'spip_territoires_extras', $where);
				if ($extras) {
					$codes_to_iso = array_column($extras, 'iso_territoire', 'valeur');
				}
			}

			// On extrait le code utilisé pour trouver son id primaire : si il n'existe pas on rejette l'item
			include_spip('inc/filtres');
			$index = $feed['mapping']['basic_fields']['code_feed'];
			$code = table_valeur($item, $index, '');
			if (
				$code
				and isset($codes_to_iso[$code])
			) {
				// On ajoute un champ dans l'item pour retrouver le code primaire
				$item['iso_feed'] = $codes_to_iso[$code];
			} else {
				// Le code primaire est introuvable, on ne retient pas cet item
				$item = [];
			}
		}
	} else {
		// Le tag type_id est absent, c'est une erreur du feed on ne retient aucun item de la source
		$item = [];
	}

	return $item;
}
