<?php
/**
 * Ce fichier contient l'action `supprimer_territoire_extra` lancée par un utilisateur autorisé pour
 * supprimer un extra de territoire créé par formulaire.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cette action permet à l'utilisateur de supprimer, de façon sécurisée,
 * un extra de territoire créé par formulaire. Cela consiste à supprimer le bloc de configuration dans la meta concernée.
 *
 * Cette action est réservée aux utilisateurs pouvant supprimer un extra de territoire.
 * Elle nécessite l'id de l'extra uniquement.
 *
 * @param null|string $arguments Arguments de l'action ou null si l'action est appelée par une URL
 *
 * @return void
 */
function action_supprimer_territoire_extra_dist(?string $arguments = null) : void {
	// Sécurisation.
	// Arguments attendus :
	// - l'identifiant de l'extra
	if (null === $arguments) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arguments = $securiser_action();
	}
	$id_extra = $arguments;

	// Verification des autorisations
	if (!autoriser('supprimer', 'territoireextra', $id_extra)) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// On supprime l'index de l'extra dans la configuration
	include_spip('inc/config');
	$config = lire_config('territoires_data', []);
	if (isset($config['extras'][$id_extra])) {
		unset($config['extras'][$id_extra]);
		ecrire_config('territoires_data', $config);
	}
}
