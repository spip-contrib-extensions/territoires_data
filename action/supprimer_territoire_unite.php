<?php
/**
 * Ce fichier contient l'action `supprimer_territoire_unite` lancée par un utilisateur autorisé pour
 * supprimer une unité de territoire créée par formulaire.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cette action permet à l'utilisateur de supprimer, de façon sécurisée,
 * une unité de territoire créée par formulaire. Cela consiste à supprimer le bloc de configuration dans la meta concernée.
 *
 * Cette action est réservée aux utilisateurs pouvant supprimer une unité de territoire.
 * Elle nécessite l'id de l'unité uniquement.
 *
 * @param null|string $arguments Arguments de l'action ou null si l'action est appelée par une URL
 *
 * @return void
 */
function action_supprimer_territoire_unite_dist(?string $arguments = null) : void {
	// Sécurisation.
	// Arguments attendus :
	// - l'identifiant de l'unité
	if (null === $arguments) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arguments = $securiser_action();
	}
	$id_unite = $arguments;

	// Verification des autorisations
	if (!autoriser('supprimer', 'territoireunite', $id_unite)) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// On supprime l'index de l'unité dans la configuration
	include_spip('inc/config');
	$config = lire_config('territoires_data', []);
	if (isset($config['unites'][$id_unite])) {
		unset($config['unites'][$id_unite]);
		ecrire_config('territoires_data', $config);
	}
}
