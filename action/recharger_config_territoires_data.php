<?php
/**
 * Ce fichier contient l'action `recharger_config_territoires_data` lancée par un utilisateur pour
 * recharger, de façon sécurisée, la configuration du plugin et des jeux de données.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cette action permet à l'utilisateur de recharger, de façon sécurisée, la configuration statique du plugin et
 * la configuration des jeux de données.
 *
 * Cette action est réservée aux utilisateurs pouvant configurer le plugin.
 * Elle ne nécessite aucun argument.
 *
 * @uses feed_charger()
 *
 * @return void
 */
function action_recharger_config_territoires_data_dist() : void {
	// Verification des autorisations : pour recharger la configuration des jeux de données on utilise la configuration
	// du plugin Territoires.
	if (!autoriser('configurer', '_territoires')) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// Rechargement de la configuration statique.
	include_spip('territoires_data_administrations');
	// -- Configuration statique du plugin Jeux de données pour Territoires
	$config_statique = territoires_data_configurer();
	// -- Ecriture de la nouvelle configuration (sans modifier le paramétrage utilisateur)
	territoires_data_adapter_configuration($config_statique);

	// On enchaine avec le rechargement des feeds : on utilise l'action existante du plugin Territoires
	$recharger_feeds = charger_fonction(
		'recharger_feeds',
		'action',
		true
	);
	$recharger_feeds('territoires_data');
}
