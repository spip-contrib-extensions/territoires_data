<?php
/**
 * Utilisations de pipelines par Territoires.
 *
 * @package    SPIP\TERRITOIRES\PIPELINES
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ajout de contenu sur certaines pages.
 *
 * Sur un objet territoire : les éventuels données statistiques complémentaires.
 * Sur la page de configuration du plugin Territoires : ajout du paramétrage des jeux de données.
 *
 * @pipeline affiche_milieu
 *
 * @param array $flux Données du pipeline
 *
 * @return array Données du pipeline modifiées pour refléter le traitement.
 */
function territoires_data_affiche_milieu(array $flux) : array {
	if (isset($flux['args']['exec'])) {
		$texte = '';

		if ($flux['args']['exec'] === 'configurer_territoires') {
			// Page de configuration du plugin Territoires (sous le formulaire existant)
			$texte .= recuperer_fond('prive/squelettes/inclure/inc-configurer');
		} else {
			// Une page objet ?
			$exec = trouver_objet_exec($flux['args']['exec']);

			if (
				$exec
				and ($exec['edition'] !== true) // page visu
				and ($id_table = $exec['id_table_objet'])
				and ($objet = $exec['type'])
				and isset($flux['args'][$id_table])
				and ($id_objet = (int) ($flux['args'][$id_table]))
			) {
				// Insertion des extras de type 'stat' chargés pour ce type de territoire
				include_spip('inc/config');
				if ($objet === 'territoire') {
					// -- acquisition de l'objet
					include_spip('action/editer_objet');
					$territoire = objet_lire('territoire', $id_objet);
					// -- Attention pour le type pays il ne faut pas passer le champ iso_pays à l'unité de peuplement
					$pays = lire_config("territoires/{$territoire['type']}/populated_by_country", false)
						? $territoire['iso_pays']
						: '';
					// -- tester si les extras 'info' sont chargées pour le type de territoire
					include_spip('inc/territoires_unite_peuplement');
					$extra_peuple = unite_peuplement_extra_est_charge($territoire['type'], $pays, 'info');
					if ($extra_peuple) {
						$texte .= recuperer_fond(
							'prive/objets/liste/territoire_extras',
							[
								'iso_territoire' => $territoire['iso_territoire'],
								'type_extra'     => 'info',
								'titre'          => _T('territoires_data:titre_liste_extras_info')
							]
						);
					}
					// -- tester si les extras 'stat' sont chargées pour le type de territoire
					$extra_peuple = unite_peuplement_extra_est_charge($territoire['type'], $pays, 'stat');
					if ($extra_peuple) {
						$texte .= recuperer_fond(
							'prive/objets/liste/territoire_extras',
							[
								'iso_territoire' => $territoire['iso_territoire'],
								'type_extra'     => 'stat',
								'titre'          => _T('territoires_data:titre_liste_extras_stat')
							]
						);
					}
				}
			}
		}

		// Insertion dans la fiche objet.
		if ($texte) {
			if ($p = strpos($flux['data'], '<!--affiche_extra-->')) {
				$flux['data'] = substr_replace($flux['data'], $texte, $p, 0);
			} elseif ($p = strpos($flux['data'], '<!--affiche_milieu-->')) {
				$flux['data'] = substr_replace($flux['data'], $texte, $p, 0);
			} else {
				$flux['data'] .= $texte;
			}
		}
	}

	return $flux;
}


/**
 * Complément à la fonction de dépeuplement des territoires.
 *
 * Le plugin supprime les feeds associées à l'unité de peuplement concernée.
 *
 * @pipeline post_depeupler_territoire
 *
 * @param array $flux Données du pipeline
 *
 * @return array Données du pipeline telles que reçues.
 */
function territoires_data_post_depeupler_territoire(array $flux) : array {
	// Vider les extras de type 'stat' pour l'unité de traitement concernée
	if (
		!empty($flux['args']['type'])
		and isset($flux['args']['pays'])
	) {
		$type = $flux['args']['type'];
		$pays = $flux['args']['pays'];

		// On vérifie que des données statistiques exitent pour l'unite de peuplement
		include_spip('inc/territoires_unite_peuplement');
		$extra_peuple = unite_peuplement_extra_est_charge($type, $pays, 'stat');
		if ($extra_peuple) {
			// Identification de la meta et de la variable de consigne
			include_spip('inc/unite_peuplement');
			$id_consigne = unite_peuplement_consigne_identifier('territoires', $type, $pays);
			if (
				include_spip('inc/config')
				and ($consigne = lire_config($id_consigne, []))
			) {
				// On récupère la liste des feeds constitutifs des jeux de données
				$feeds = $consigne['sta'];

				// On supprime les extras de ces jeux de données
				$where = sql_in('feed_id', $feeds);
				sql_delete('spip_territoires_extras', $where);
			}

		}

	}

	return $flux;
}
