<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/territoires_data-paquet-xml-territoires_data?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'territoires_data_description' => 'This plugin provides a mechanism derived from the Factory Mashup plugin for associating data and statistics with territories provided by the Territories plugin. It provides the basis for creating choropleth maps.', # MODIF
	'territoires_data_nom' => 'Datasets for territories', # MODIF
	'territoires_data_slogan' => 'Associating data and statistics with territories' # MODIF
);
