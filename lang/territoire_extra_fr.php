<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/territoires_data.git
return [
	// C
	'champ_extra_format' => 'Format',
	'champ_extra_id' => 'Id',
	'champ_extra_titre' => 'Libellé',
	'champ_extra_type' => 'Type',
	'champ_extra_unite' => 'Unité',
	'champ_unite_id' => 'Id',
	'champ_unite_titre' => 'Libellé',

	// D
	'discretisation_equivalence' => 'égales étendues',
	'discretisation_jenks' => 'variance de Jenks',
	'discretisation_moyenne_emboitee' => 'moyennes emboîtées',
	'discretisation_progression_arithmetique' => 'progression arithmétique',
	'discretisation_progression_geometrique' => 'progression géométrique',
	'discretisation_quantile' => 'quantiles',
	'discretisation_standard' => 'moyenne et écart-type',
	'discretisation_standard_k' => 'moyenne et écart-type adapté',
	'discretisation_standard_cote_z' => 'moyenne et écart-type cote Z',

	// E
	'extra_area' => 'superficie',
	'extra_capital' => 'capitale',
	'extra_date_creation' => 'date de création',
	'extra_latitude' => 'latitude',
	'extra_longitude' => 'longitude',
	'extra_phone_id' => 'indicatif téléphonique',
	'extra_population' => 'population',
	'extra_tld' => 'extension',

	// F
	'format_extra_date' => 'date',
	'format_extra_float' => 'décimal',
	'format_extra_integer' => 'entier',
	'format_extra_string' => 'chaine',

	// I
	'info_1_extra' => '1 nature de données',
	'info_1_unite' => '1 unité',
	'info_extra_aucun' => 'Utilisez les boutons pour créer de nouvelles natures de données',
	'info_nb_extra' => '@nb@ natures de données',
	'info_nb_unite' => '@nb@ unités',
	'info_transformation_aucune' => '— aucune —',
	'info_unite_aucune' => 'Utilisez les boutons pour créer de nouvelles unites',

	// T
	'titre_extra' => 'nature des données',
	'transformation_abbr_inverse' => '1/x',
	'transformation_abbr_exponentiel' => 'exp',
	'transformation_abbr_10exponentiel' => '10<sup>x</sup>',
	'transformation_abbr_logarithme' => 'ln',
	'transformation_abbr_logarithme10' => 'log₁₀',
	'transformation_abbr_puissance2' => 'x²',
	'transformation_abbr_puissance3' => 'x³',
	'transformation_abbr_racine2' => '√x',
	'transformation_abbr_racine3' => '∛x',
	'transformation_inverse' => 'inverse',
	'transformation_exponentiel' => 'exponentiel',
	'transformation_10exponentiel' => 'puissance de 10',
	'transformation_logarithme' => 'logarithme',
	'transformation_logarithme10' => 'logarithme base 10',
	'transformation_puissance2' => 'carré',
	'transformation_puissance3' => 'cube',
	'transformation_racine2' => 'racine carrée',
	'transformation_racine3' => 'racine cubique',
	'type_extra_info' => 'caractéristique',
	'type_extra_stat' => 'statistique',

	// U
	'unite_hab' => 'habitants',
	'unite_km2' => 'km²'
];
