<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/territoires_data.git
return [
	// T
	'territoires_data_description' => 'Ce plugin fournit un mécanisme issu du plugin Factory Mashup pour associer de multiples données et statistiques aux différents territoires fournis par le plugin Territoires. Il fournit ainsi les bases nécessaires à l’étude et la représentation des données géographiques (statistique descriptive, création de cartes choroplèthes, etc.).',
	'territoires_data_nom' => 'Jeux de données pour territoires',
	'territoires_data_slogan' => 'Associer des données et statistiques aux territoires'
];
