<?php
/**
 * Ce fichier contient les fonctions d'autorisations du plugin.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction appelée par le pipeline.
 */
function territoires_data_autoriser() {
}

/**
 * Autorisation de créer des extras de territoires.
 * Il faut :
 * - posséder l'autorisation de créer des feeds de territoires.
 *
 * @param string         $faire   L'action : `creer`
 * @param string         $type    Le type d'objet ou nom de table : `territoireextra` (ce n'est pas un objet au sens SPIP)
 * @param null|string    $id      Id de l'objet sur lequel on veut agir : '', inutilisé
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
 */
function autoriser_territoireextra_creer_dist($faire, $type, $id, $qui, $options) {
	return
		autoriser('creer', 'feed', null, $qui, ['plugin' => 'territoires_data']);
}

/**
 * Autorisation d'édition pour les extras de territoires éditables.
 * Il faut :
 * - posséder l'autorisation de créer un extra
 * - fournir un identifiant d'extra existant
 * - et que l'extra soit éditable.
 *
 * @param string         $faire   Action demandée : `modifier` (pour éditer)
 * @param string         $type    Type d'objet sur lequel appliquer l'action : `territoireextra`(ce n'est pas un objet au sens SPIP)
 * @param null|string    $id      Identifiant de l'objet : celui de l'extra sur lequel appliquer l'action
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
**/
function autoriser_territoireextra_editer_dist($faire, $type, $id, $qui, $options) {
	// Initialisation de l'autorisation à non autorisé par défaut.
	$autorise = false;

	if (
		autoriser('creer', 'feed', '', $qui, ['plugin' => 'territoires_data'])
		and $id
		and is_string($id)
		and include_spip('inc/config')
		and ($extra = lire_config("territoires_data/extras/{$id}", []))
		and $extra['is_editable']
	) {
		$autorise = true;
	}

	return $autorise;
}

/**
 * Autorisation de suppression d'un extra de territoires éditable.
 * Il faut :
 * - posséder l'autorisation de modifier un extra
 * - et que l'extra ne soit pas utilisé.
 *
 * @uses autoriser_territoireextra_supprimer_dist()
 *
 * @param string         $faire   Action demandée : `supprimer`
 * @param string         $type    Type d'objet sur lequel appliquer l'action : `territoireextra`(ce n'est pas un objet au sens SPIP)
 * @param null|string    $id      Identifiant de l'objet : celui de l'extra sur lequel appliquer l'action
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
**/
function autoriser_territoireextra_supprimer_dist($faire, $type, $id, $qui, $options) {
	// Initialisation de l'autorisation à non autorisé par défaut.
	$autorise = false;

	if (autoriser('editer', 'territoireextra', $id, $qui, $options)) {
		// On vérifier qu'il n'existe pas de feed (peuplé ou pas) utilisant cet extra.
		// Pour ce faire, on vérifie le champ `extra` des static_fields du mapping pour les feeds du plugin uniquement
		// -- on utilise pas l'API feed_repertorier car elle fournit les champs sans décodage, donc on y gagne rien
		$where = [
			'plugin=' . sql_quote('territoires_data'),
			'target_id=' . sql_quote('territoires_extras'),
		];
		$extra_utilise = false;
		if ($mappings = sql_allfetsel('mapping', 'spip_feeds', $where)) {
			foreach ($mappings as $_mapping) {
				$mapping = json_decode($_mapping['mapping'], true);
				if (
					isset($mapping['static_fields'][$id])
					and ($mapping['static_fields'][$id] === $id)
				) {
					$extra_utilise = true;
					break;
				}
			}
		}
		if (!$extra_utilise) {
			$autorise = true;
		}
	}

	return $autorise;
}

/**
 * Autorisation de créer des unites pour les extras de territoires.
 * Il faut :
 * - posséder l'autorisation de créer des feeds de territoires.
 *
 * @param string         $faire   L'action : `creer`
 * @param string         $type    Le type d'objet ou nom de table : `territoireunite` (ce n'est pas un objet au sens SPIP)
 * @param null|string    $id      Id de l'objet sur lequel on veut agir : '', inutilisé
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
 */
function autoriser_territoireunite_creer_dist($faire, $type, $id, $qui, $options) {
	return
		autoriser('creer', 'feed', null, $qui, ['plugin' => 'territoires_data']);
}

/**
 * Autorisation de modifier les unites de territoires éditables.
 * Il faut :
 * - posséder l'autorisation de créer un feed
 * - fournir un identifiant d'unité existant
 * - et que l'unité soit éditable.
 *
 * @param string         $faire   Action demandée : `modifier` (pour éditer)
 * @param string         $type    Type d'objet sur lequel appliquer l'action : `territoireunite`(ce n'est pas un objet au sens SPIP)
 * @param null|string    $id      Identifiant de l'objet : celui de l'unité sur laquelle appliquer l'action
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
**/
function autoriser_territoireunite_editer_dist($faire, $type, $id, $qui, $options) {
	// Initialisation de l'autorisation à non autorisé par défaut.
	$autorise = false;

	if (
		autoriser('creer', 'feed', '', $qui, ['plugin' => 'territoires_data'])
		and $id
		and is_string($id)
		and include_spip('inc/config')
		and ($unite = lire_config("territoires_data/unites/{$id}", []))
		and $unite['is_editable']
	) {
		$autorise = true;
	}

	return $autorise;
}

/**
 * Autorisation de suppression d'un extra de territoires éditable.
 * Il faut :
 * - posséder l'autorisation de modifier un extra
 * - et que l'extra ne soit pas utilisé.
 *
 * @uses autoriser_territoireextra_supprimer_dist()
 *
 * @param string         $faire   Action demandée : `supprimer`
 * @param string         $type    Type d'objet sur lequel appliquer l'action : `territoireunite`(ce n'est pas un objet au sens SPIP)
 * @param null|string    $id      Identifiant de l'objet : celui de l'unité sur laquelle appliquer l'action
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
**/
function autoriser_territoireunite_supprimer_dist($faire, $type, $id, $qui, $options) {
	// Initialisation de l'autorisation à non autorisé par défaut.
	$autorise = false;

	if (autoriser('editer', 'territoireunite', $id, $qui, $options)) {
		// On vérifier qu'il n'existe pas d'extra utilisant cette unité.
		// Pour ce faire, on vérifie le champ `unite` de la liste des extras incluses dans la meta de configuration.
		include_spip('inc/config');
		$extras = lire_config('territoires_data/extras', []);
		$unite_utilisee = false;
		foreach ($extras as $_extra) {
			if ($_extra['unite'] === $id) {
				$unite_utilisee = true;
				break;
			}
		}
		if (!$unite_utilisee) {
			$autorise = true;
		}
	}

	return $autorise;
}

/**
 * Autorisation, pour tous les feeds de statistiques, d'analyser les données d'un feed (se rendre vers une page
 * d'analyse statistique). Il faut :
 * - que la configuration du plugin permette l'analyse
 * - posséder l'autorisation minimale `ezmashup`
 * - fournir un identifiant de feed existant
 * - que le feed soit actif et que son type d'extra soit `stat` (Statistiques)
 * - que le feed soit peuplé
 *
 * @param string         $faire   Action demandée : `analyser`
 * @param string         $type    Type d'objet sur lequel appliquer l'action : `feed`
 * @param int            $id      Identifiant de l'objet : celui du feed sur lequel appliquer l'action
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `plugin`, préfixe du plugin utilisateur
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
**/
function autoriser_feed_analyser_dist($faire, $type, $id, $qui, $options) {
	// Initialisation de l'autorisation à non autorisé par défaut.
	$autorise = false;

	if (
		include_spip('inc/config')
		and lire_config('territoires_data/parametres/autoriser_analyse', '')
		and autoriser('ezmashup', $type, $id, $qui, $options)
		and $id
		and is_string($id)
		and include_spip('inc/ezmashup_feed')
		and ($feed = feed_lire($options['plugin'], $id))
		and ($feed['is_active'] === 'oui')
		and ($feed['category'] === 'territory_stat')
		and include_spip('inc/ezmashup_dataset_target')
		and dataset_target_informer($options['plugin'], $id)
	) {
		$autorise = true;
	}

	return $autorise;
}
