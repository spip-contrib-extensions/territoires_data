<?php
/**
 * Gestion du formulaire de création et d'édition d'une unité de territoires.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Chargement du formulaire de création et d'édition d'une unité de territoires.
 *
 * @param null|string $id_unite Identificant de l'unité d'extra (édition) ou vide (création)
 * @param null|string $redirect URL de redirection en sortie de formulaire
 *
 * @return array Environnement du formulaire
 */
function formulaires_editer_territoire_unite_charger_dist(?string $id_unite = 'new', ?string $redirect = '') : array {
	// Initialisation du contexte du formulaire pour une création
	$valeurs = [
		'unite_id' => $id_unite,
		'label' => '',
		'_edition' => '',
		'editable' => true
	];

	// Si édition, on charge l'extra
	if ($id_unite !== 'new') {
		include_spip('inc/config');
		$extra = lire_config("territoires_data/unites/{$id_unite}", []);
		$valeurs = array_merge($valeurs, $extra);

		// On passe en mode édition
		$valeurs['_edition'] = 'oui';
	}

	return $valeurs;
}

/**
 * Vérifications du formulaire de création et d'édition d'une unité de territoires.
 * En particulier, on ne peut pas créer une unité avec le même id.
 *
 * @param null|string $id_unite Identificant de l'unité d'extra (édition) ou vide (création)
 * @param null|string $redirect URL de redirection en sortie de formulaire
 *
 * @return array Tableau des erreurs
 */
function formulaires_editer_territoire_unite_verifier_dist(?string $id_unite = 'new', ?string $redirect = '') : array {
	// Par défaut, aucune erreur
	$erreurs = [];

	// On ne cherche pas les champs obligatoires qui sont tous gérés par le formulaire, mais les erreurs suivantes:
	// - id d'unité déjà existant ou toujours appelé `new`
	// - un id mal formé
	if ($id_unite === 'new') {
		// inutile de vérifier un id si c'est une modification car il n'est pas modifiable
		$id = _request('unite_id');
		if (
			include_spip('inc/config')
			and (lire_config("territoires_data/unites/{$id}"))
		) {
			$erreurs['unite_id'] = _T('territoires_data:erreur_id_existe');
		} elseif ($id === 'new') {
			$erreurs['unite_id'] = _T('territoires_data:erreur_id_new');
		} elseif (!preg_match('#^[\w]+$#i', $id)) {
			$erreurs['unite_id'] = _T('territoires_data:erreur_id_invalide');
		}
	}

	return $erreurs;
}

/**
 * Traitement du formulaire de création et d'édition d'une unité de territoires.
 *
 * @param null|string $id_unite Identificant de l'unité d'extra (édition) ou vide (création)
 * @param null|string $redirect URL de redirection en sortie de formulaire
 *
 * @return array Retours des traitements
 */
function formulaires_editer_territoire_unite_traiter_dist(?string $id_unite = 'new', ?string $redirect = '') : array {
	// Initialisation du retour de la fonction
	$retour = [];

	// On récupère les saisies et on initialise la configuration de l'extra
	$unite_id = _request('unite_id');
	$unite = [
		'label'     => _request('label'),
		'is_editable' => true,
	];

	// Ajout ou mise à jour de l'unité dans la meta de configuration
	include_spip('inc/config');
	ecrire_config("territoires_data/unites/{$unite_id}", $unite);

	// Redirection vers la page demandée si tout s'est bien passé
	if (
		empty($retour['message_erreur'])
		and $redirect
	) {
		$retour['redirect'] = $redirect;
	}
	$retour['editable'] = true;

	return $retour;
}
