<?php
/**
 * Gestion du formulaire d'édition d'une discrétisation d'un feed.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Chargement du formulaire d'édition d'une discrétisation d'un feed.
 *
 * @param string $plugin  Préfixe du plugin fournissant le feed
 * @param string $id_feed Identificant du feed dont est issue la série à discrétiser
 *
 * @return array Environnement du formulaire
 */
function formulaires_editer_discretisation_charger_dist(string $plugin, string $id_feed) {
	$valeurs = [];

	// Récupération des informations sur le feed
	include_spip('inc/ezmashup_feed');
	$feed = feed_lire('territoires_data', $id_feed);

	// Défaut de la méthode : dépend de la distribution
	$valeurs['_methode_defaut'] = 'equivalence';

	// Nombre de classes possibles, valeur par défaut et indices sur le nombre optimal de classes
	include_spip('inc/config');
	$config_classe = lire_config('territoires_data/classes', []);
	foreach ($config_classe['nb_liste'] as $_nb_classe) {
		$valeurs['_classes'][$_nb_classe] = strval($_nb_classe);
	}
	$valeurs['_classe_defaut'] = $config_classe['nb_defaut'];

	include_spip('territoires_data_fonctions');
	$indices = territoire_feed_aider_discretisation($plugin, $id_feed);
	$explication = label_ponctuer(_T('territoires_data:explication_indication_nbclasses'));
	$explication .= '<br>';
	foreach ($indices as $_indice => $_valeur) {
		$explication .= '<span style="padding-left:1em;">' . _T("territoires_data:label_serie_nbclasse_{$_indice}") . ' ' . $_valeur . '</span><br>';
	}
	$valeurs['_explication_classe'] = $explication;

	// Récupération des saisies si besoin
	$valeurs['transformation'] = _request('transformation') ?? '';
	$valeurs['nb_classes'] = _request('nb_classes');
	$valeurs['methode'] = _request('methode') ?? '';

	// Passer les variables d'environnement du feed
	$valeurs['plugin'] = $plugin;
	$valeurs['feed_id'] = $id_feed;
	$valeurs['extra'] = territoire_feed_lire_extra($plugin, $id_feed);

	return $valeurs;
}

/**
 * Vérifications du formulaire d'édition d'une discrétisation d'un feed.
 *
 * @param string $plugin  Préfixe du plugin fournissant le feed
 * @param string $id_feed Identificant du feed dont est issue la série à discrétiser
 *
 * @return array Tableau des erreurs
 */
function formulaires_editer_discretisation_verifier_dist(string $plugin, string $id_feed) {
	$erreurs = [];

	// Récupérer les saisies
	$methode = _request('methode');
	$nb_classes = (int) _request('nb_classes');

	// Si la méthode est celle des moyennes emboitées alors seule une puissance de 2 est valide comme nombre de classes
	if (
		($methode === 'moyenne_emboitee')
		&& (($nb_classes & ($nb_classes - 1)) != 0)
	) {
		$erreurs['nb_classes'] = 'erreur';
	}

	return $erreurs;
}

/**
 * Traitement du formulaire d'édition d'une discrétisation d'un feed.
 *
 * @param string $plugin  Préfixe du plugin fournissant le feed
 * @param string $id_feed Identificant du feed dont est issue la série à discrétiser
 *
 * @return array Retours des traitements
 */
function formulaires_editer_discretisation_traiter_dist(string $plugin, string $id_feed) {
	// Initialisation du retour de la fonction
	$retour = [];

	// Détermination de l'action
	$action =  _request('evaluer') ? 'evaluer' : 'enregistrer';

	// Création de la discrétisation en fonction des paramètres
	// -- acquisition des paramètres
	$id_transformation = _request('transformation');
	$nb_classes = (int) _request('nb_classes');
	$methode = _request('methode');

	// -- discrétisation
	include_spip('territoires_data_fonctions');
	$discretisation = territoire_feed_discretiser_serie($plugin, $id_feed, $methode, $nb_classes, $id_transformation);
	if ($discretisation) {
		if ($action === 'evaluer') {
			// Renvoie la discretisation dans le message ok
			$retour['message_ok']['discretisation']	= $discretisation;
			$retour['message_ok']['resume']	= 'Evaluation ok';
			$retour['editable']	= true;
		} else {
			// Enregistre la discretisation dans le feed
		}
	} else {
		$retour['message_erreur'] = 'erreur';
	}

	return $retour;
}
