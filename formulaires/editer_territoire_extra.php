<?php
/**
 * Gestion du formulaire de création et d'édition d'un extra de territoires.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Chargement du formulaire de création et d'édition d'un extra de territoires.
 *
 * @param null|string $id_extra Identificant de la nature d'extra (édition) ou vide (création)
 * @param null|string $redirect URL de redirection en sortie de formulaire
 *
 * @return array Environnement du formulaire
 */
function formulaires_editer_territoire_extra_charger_dist(?string $id_extra = 'new', ?string $redirect = '') : array {
	// Initialisation du contexte du formulaire pour une création
	$valeurs = [
		'extra_id' => $id_extra,
		'type_extra' => 'stat',
		'label' => '',
		'format' => 'integer',
		'extra_decimale' => '',
		'unite' => '',
		'_edition' => '',
		'editable' => true
	];

	// Si édition, on charge l'extra
	if ($id_extra !== 'new') {
		include_spip('inc/config');
		$extra = lire_config("territoires_data/extras/{$id_extra}", []);
		$valeurs = array_merge($valeurs, $extra);

		// On détermine le nombre de décimales
		if (strpos($extra['format'], 'float') !== false) {
			$valeurs['format'] = 'float';
			$valeurs['extra_decimale'] = str_replace('float', '', $extra['format']);
		}

		// On passe en mode édition
		$valeurs['_edition'] = 'oui';
	}

	return $valeurs;
}

/**
 * Vérifications du formulaire de création et d'édition d'un extra de territoires.
 * En particulier, on ne peut pas créer un extra avec le même id.
 *
 * @param null|string $id_extra Identificant de la nature d'extra (édition) ou vide (création)
 * @param null|string $redirect URL de redirection en sortie de formulaire
 *
 * @return array Tableau des erreurs
 */
function formulaires_editer_territoire_extra_verifier_dist(?string $id_extra = 'new', ?string $redirect = '') : array {
	// Par défaut, aucune erreur
	$erreurs = [];

	// On ne cherche pas les champs obligatoires qui sont tous gérés par le formulaire, mais les erreurs suivantes:
	// - id d'extra déjà existant ou toujours appelé `new`
	// - un id mal formé
	if ($id_extra === 'new') {
		// inutile de vérifier un id si c'est une modification car il n'est pas modifiable
		$id = _request('extra_id');
		if (
			include_spip('inc/config')
			and (lire_config("territoires_data/extras/{$id}"))
		) {
			$erreurs['extra_id'] = _T('territoires_data:erreur_id_existe');
		} elseif ($id === 'new') {
			$erreurs['extra_id'] = _T('territoires_data:erreur_id_new');
		} elseif (!preg_match('#^[\w]+$#i', $id)) {
			$erreurs['extra_id'] = _T('territoires_data:erreur_id_invalide');
		}
	}

	// On vérifie que le nombre de décimales est compris entre 1 et 9
	if (_request('format') === 'float') {
		$decimale = (int) _request('extra_decimale');
		if (
			($decimale < 1)
			or ($decimale > 9)
		) {
			$erreurs['extra_decimale'] = _T('territoires_data:erreur_extra_decimale');
		}
	}

	return $erreurs;
}

/**
 * Traitement du formulaire de création et d'édition d'un extra de territoires.
 *
 * @param null|string $id_extra Identificant de la nature d'extra (édition) ou vide (création)
 * @param null|string $redirect URL de redirection en sortie de formulaire
 *
 * @return array Retours des traitements
 */
function formulaires_editer_territoire_extra_traiter_dist(?string $id_extra = 'new', ?string $redirect = '') : array {
	// Initialisation du retour de la fonction
	$retour = [];

	// On récupère les saisies et on initialise la configuration de l'extra
	$extra_id = _request('extra_id');
	$extra = [
		'label'     => _request('label'),
		'is_editable' => true,
		'type_extra'    => _request('type_extra'),
		'format' => _request('format'),
		'unite' => _request('unite'),
	];

	// On détermine les décimales
	$decimales = '';
	if ($extra['format'] === 'float') {
		$decimales = _request('extra_decimale');
	}
	$extra['format'] .= $decimales;

	// Ajout ou mise à jour de l'extra dans la meta de configuration
	include_spip('inc/config');
	ecrire_config("territoires_data/extras/{$extra_id}", $extra);

	// Redirection vers la page demandée si tout s'est bien passé
	if (
		empty($retour['message_erreur'])
		and $redirect
	) {
		$retour['redirect'] = $redirect;
	}
	$retour['editable'] = true;

	return $retour;
}
