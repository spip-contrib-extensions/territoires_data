<?php
/**
 * Gestion du formulaire d'édition d'un feed de territoires.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Chargement du formulaire d'édition d'un feed de territoires.
 *
 * @param string      $id_feed  Identificant du feed servant à l'affichage du bloc de détails
 * @param null|string $redirect URL de redirection suite au traitement d'édition ok. Si vide on revient sur l'admin
 *
 * @return array Environnement du formulaire
 */
function formulaires_editer_territoire_feed_charger_dist(string $id_feed, ?string $redirect = '') {
	// Récupération des informations sur le feed
	include_spip('inc/ezmashup_feed');
	$feed = feed_lire('territoires_data', $id_feed);

	// Identification du feed
	include_spip('ezmashup_fonctions');
	$valeurs = [
		'title'         => $feed['title'],
		'description'   => $feed['description'],
		'nom_categorie' => typo(feed_categorie_repertorier('territoires_data', $feed['category'], 'name'))
	];

	// Types de code possibles pour les territoires concernés
	$type_code = $feed['tags']['_type_id'];
	$types_code['iso_territoire'] = _T('territoire:champ_iso_territoire_label') . ' (iso_territoire)';
	$where = [
		'type_extra=' . sql_quote('code'),
		'type=' . sql_quote($feed['tags']['type']),
	];
	if ($feed['tags']['pays']) {
		$where[] = 'iso_pays=' . sql_quote($feed['tags']['pays']);
	}
	$ids_code = sql_allfetsel('extra', 'spip_territoires_extras', $where, 'extra');
	if ($ids_code) {
		foreach (array_column($ids_code, 'extra') as $_code) {
			$types_code[$_code] = _T('territoire_extra:extra_' . $_code) . " ({$_code})";
		}
	}
	$valeurs['type_code'] = $type_code;
	$valeurs['_types_code'] = $types_code;

	// Format et décodage de la source
	$source = $feed['sources_basic']['basic_1'];
	if ($type_code === 'iso_territoire') {
		$valeurs['mapping_code'] = $feed['mapping']['basic_fields']['iso_territoire'];
	} else {
		$valeurs['mapping_code'] = $feed['mapping']['basic_fields']['code_feed'];
	}
	$valeurs['mapping_valeur'] = $feed['mapping']['basic_fields']['valeur'];
	$format = $source['source']['format'];
	if ($format === 'csv') {
		$valeurs['decodage'] = $source['decoding']['delimiter'];
		$valeurs['_label_decodage'] = _T('territoires_data:label_feed_decodage_delimiteur');
		$valeurs['_explication_decodage'] = _T('territoires_data:explication_feed_decodage_delimiteur');

		$valeurs['_explication_mapping_code'] = _T('territoires_data:explication_feed_mapping_code_csv');
		$valeurs['_explication_mapping_valeur'] = _T('territoires_data:explication_feed_mapping_valeur_csv');
	} else {
		$valeurs['decodage'] = $source['decoding']['root_node'];
		$valeurs['_label_decodage'] = _T('territoires_data:label_feed_decodage_racine');
		$valeurs['_explication_decodage'] = _T('territoires_data:explication_feed_decodage_racine');

		$valeurs['_explication_mapping_code'] = _T('territoires_data:explication_feed_mapping_code');
		$valeurs['_explication_mapping_valeur'] = _T('territoires_data:explication_feed_mapping_valeur');
	}

	// Crédits et licence de la source
	$valeurs['credit_date'] = $source['source']['last_update'];
	$valeurs['credit_version'] = $source['source']['version'];
	$valeurs['credit_licence'] = $source['source']['license'];
	$valeurs['fournisseur'] = $source['provider']['name'];
	$valeurs['fournisseur_url'] = $source['provider']['url'];

	return $valeurs;
}

/**
 * Vérifications du formulaire d'édition d'un feed de territoires.
 *
 * @param string      $id_feed  Identificant du feed servant à l'affichage du bloc de détails
 * @param null|string $redirect URL de redirection suite au traitement d'édition ok. Si vide on revient sur l'admin
 *
 * @return array Tableau des erreurs
 */
function formulaires_editer_territoire_feed_verifier_dist(string $id_feed, ?string $redirect = '') {
	$erreurs = [];

	// Récupération des informations sur le feed
	include_spip('inc/ezmashup_feed');
	$feed = feed_lire('territoires_data', $id_feed);
	$source = $feed['sources_basic']['basic_1'];
	$format_source = $source['source']['format'];

	// On collecte les paramètres
	$decodage = _request('decodage');
	$mapping_code = _request('mapping_code');
	$mapping_valeur = _request('mapping_valeur');

	// Vérification de la saisie des mappings qui ne doivent contenir qu'un ou plusieurs mots séparés par des '/'
	if ($format_source !== 'csv') {
		if (!preg_match('#^[\w/]+$#i', $mapping_code)) {
			$erreurs['mapping_code'] = _T('territoires_data:erreur_feed_mapping_code');
		}
		if (!preg_match('#^[\w/]+$#i', $mapping_valeur)) {
			$erreurs['mapping_valeur'] = _T('territoires_data:erreur_feed_mapping_valeur');
		}
	}

	// Vérification du décodage en fonction du type de source
	// -- si fichier csv : on attend un délimiteur parmi ',', ';', '|' et '\t' ou vide (la valeur par défaut est la virgule)
	// -- sinon : on attend un index comme pour le mapping ou vide
	if ($decodage) {
		if ($format_source === 'csv') {
			if (!in_array($decodage, [',', ';', "\t"])) {
				$erreurs['decodage'] = _T('territoires_data:erreur_feed_decodage');
			}
		} elseif (!preg_match('#^[\w/]+$#i', $decodage)) {
			$erreurs['decodage'] = _T('territoires_data:erreur_feed_mapping_code');
		}
	}

	// Vérifier l'URL du provider si saisie
	$url_provider = _request('provider_url');
	if (
		$url_provider
		and include_spip('inc/distant')
		and (valider_url_distante($url_provider) === false)
	) {
		// URL distante non valide.
		// TODO : voir aussi filter_var(url, FILTER_VALIDATE_URL)
		$erreurs['provider_url'] = _T('territoires_data:erreur_feed_url_provider');
	}

	// Vérifier l'URL du build si saisie
	if ($source['source']['type'] === 'file') {
		$url_build = _request('build_from');
		if (
			$url_build and include_spip('inc/distant') and (valider_url_distante($url_build) === false)
		) {
			// URL distante non valide.
			// TODO : voir aussi filter_var(url, FILTER_VALIDATE_URL)
			$erreurs['provider_url'] = _T('territoires_data:erreur_feed_url_build');
		}
	}

	return $erreurs;
}

/**
 * Traitement du formulaire d'édition d'un feed de territoires.
 *
 * @param string      $id_feed  Identificant du feed servant à l'affichage du bloc de détails
 * @param null|string $redirect URL de redirection suite au traitement d'édition ok. Si vide on revient sur l'admin
 *
 * @return array Retours des traitements
 */
function formulaires_editer_territoire_feed_traiter_dist(string $id_feed, ?string $redirect = '') {
	// Initialisation du retour de la fonction
	$retour = [];

	// Désactivation de l'ajax car on redirige systématiquement
	refuser_traiter_formulaire_ajax();

	// Récupération des variables constitutives du YAML
	include_spip('inc/charsets');
	$titre_feed = _request('title');
	$desc_feed = _request('description');
	$type_code = _request('type_code');
	$decodage = trim(_request('decodage')) ?? '';
	$mapping_code = translitteration(_request('mapping_code'));
	$mapping_valeur = translitteration(_request('mapping_valeur'));

	// Initialisation du YAML à partir du template
	include_spip('inc/ezmashup_feed');
	$ressource = [
		'type' => 'config',
	];
	$description_yaml = feed_ressource_lire('territoires_data', $id_feed, $ressource);

	if ($description_yaml !== false) {
		// Personnalisation du YAML en fonction du contexte des saisies.
		// On considère que toutes les vérifications ont été faites et que donc les variables saisies sont cohérentes
		// -- identification
		$description_yaml['title'] = $titre_feed;
		$description_yaml['description'] = $desc_feed;
		// -- mapping : basic, static et unused fields
		if ($type_code !== $description_yaml['tags']['_type_id']) {
			// -- catégorisation : seuls le tag du type de code est modifiable
			$description_yaml['tags']['_type_id'] = $type_code;
			if ($type_code === 'iso_territoire') {
				// Le code primaire est utilisé dans jeu de données, il n'est pas utile de passer par un code alternatif
				// - le champ `code_feed` est donc inutile
				$description_yaml['mapping']['basic_fields']['iso_territoire'] = $mapping_code;
				unset($description_yaml['mapping']['basic_fields']['code_feed'], $description_yaml['mapping']['unused_fields']);
			} else {
				// Le jeu de données utilise un code alternatif au code primaire
				// - le champ `code_feed` est donc nécessaire pour l'extraction mais ne sera pas conservé in fine
				$description_yaml['mapping']['basic_fields']['code_feed'] = $mapping_code;
				$description_yaml['mapping']['basic_fields']['iso_territoire'] = 'iso_feed';
				$description_yaml['mapping']['unused_fields'] = ['code_feed'];
			}
		}
		$description_yaml['mapping']['basic_fields']['valeur'] = $mapping_valeur;
		// -- la source : pour l'instant les crédits ne sont pas supportés, elle porte toujours l'id `source_1`
		//    L'uri sera remplie plus avant pour traiter le cas du fichier télécharger à copier dans l'emplacement final
		$id_source = 'basic_1';
		if ($description_yaml['sources_basic'][$id_source]['source']['format'] === 'csv') {
			$description_yaml['sources_basic'][$id_source]['decoding']['delimiter'] = $decodage;
		} else {
			$description_yaml['sources_basic'][$id_source]['decoding']['root_node'] = $decodage;
		}
		// -- Les crédits & licence
		$description_yaml['sources_basic'][$id_source]['source']['license'] = _request('credit_licence') ?? '';
		$description_yaml['sources_basic'][$id_source]['source']['version'] = _request('credit_version') ?? '';
		$description_yaml['sources_basic'][$id_source]['source']['last_update'] = _request('credit_date') ?? '';
		$description_yaml['sources_basic'][$id_source]['provider']['name'] = _request('fournisseur') ?? '';
		$description_yaml['sources_basic'][$id_source]['provider']['url'] = _request('fournisseur_url') ?? '';

		// Ecriture du YAML
		$ressource = [
			'type' => 'config',
		];
		if (feed_ressource_ecrire('territoires_data', $id_feed, $ressource, $description_yaml)) {
			// Insertion du YAML en base de données
			feed_charger('territoires_data');
		} else {
			$retour['message_erreur'] = _T('territoires_data:erreur_ecriture_config');
		}
	} else {
		$retour['message_erreur'] = _T('territoires_data:erreur_lecture_config');
	}

	// Redirection vers la page des jeux de données si tout s'est bien passé
	if (empty($retour['message_erreur'])) {
		$retour['redirect'] = $redirect
			?: parametre_url(
				parametre_url(
					generer_url_ecrire(
						'peupler_data'
					),
					'composant',
					'jeu'
				),
				'category',
				$description_yaml['category']
			);
	}

	return $retour;
}
