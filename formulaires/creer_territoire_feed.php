<?php
/**
 * Gestion du formulaire (assistant) de création d'un feed de données pour des territoires.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Chargement des données :.
 *
 * @param string      $id_categorie Identifiant de la catégorie des feeds. Permet de déterminer le type d'extra.
 * @param null|string $redirect     URL de retour suite à un traitement ok ou vide sinon. Si vide on renvoie sur l'admin
 *
 * @return array Tableau des données à charger par le formulaire.
 */
function formulaires_creer_territoire_feed_charger(string $id_categorie, string $redirect = '') : array {
	// Etape 1
	// Récupération des saisies éventuelles de l'étape 1
	$valeurs = [
		'type'        => _request('type') ?? '',
		'type_source' => _request('type_source') ?? '',
		'_fichiers'   => _request('_fichiers') ?? '',
		'url_source'  => _request('url_source') ?? '',
		'extra'       => _request('extra') ?? '',
		'categorie'   => $id_categorie ?? (_request('categorie') ?? ''), // TODO : vérifier si c'est pas category ?
	];
	include_spip('inc/config');
	$types_territoire = lire_config('territoires/types');
	foreach ($types_territoire as $_type) {
		if (lire_config("territoires/{$_type}/populated_by_country", false)) {
			$variable_pays = 'pays_' . $_type;
			$valeurs[$variable_pays] = _request($variable_pays) ?? '';
		}
	}

	// Initialisation des paramètres de l'unité de peuplement
	// -- Liste des types de territoire.
	include_spip('inc/config');
	$types_territoire = lire_config('territoires/types');
	foreach ($types_territoire as $_type) {
		$valeurs['_types_territoire'][$_type] = spip_ucfirst(_T("territoire:type_{$_type}"));
	}
	$valeurs['_type_territoire_defaut'] = 'country';

	// -- Détermination des pays disponibles pour les types subdivision, infrasubdivision et protected_area
	//    et structuration des colonnes du choix radio.
	$select = ['t1.iso_pays', 't2.nom_usage'];
	$from = ['spip_territoires AS t1', 'spip_territoires AS t2'];
	$where = ['t2.iso_territoire=t1.iso_pays'];
	foreach ($types_territoire as $_type) {
		if (lire_config("territoires/{$_type}/populated_by_country", false)) {
			$where[1] = 't1.type=' . sql_quote($_type);
			$pays = sql_allfetsel($select, $from, $where, 't1.iso_pays');
			$valeurs['_pays'][$_type] = array_column($pays, 'nom_usage', 'iso_pays');
			$valeurs['_choix_multi_col'][$_type] = 1 + intdiv(count($valeurs['_pays'][$_type]), 5);
			if ($valeurs['_choix_multi_col'][$_type] > 3) {
				$valeurs['_choix_multi_col'][$_type] = 3;
			}
			$valeurs['_classe_conteneur'][$_type] = ($valeurs['_choix_multi_col'][$_type] > 1)
				? 'pleine_largeur'
				: '';
		}
	}

	// Initialisation des paramètres de la source du jeu de données
	// -- Type de source
	$valeurs['_types_source'] = [
		'api'  => _T('territoires_data:option_feed_type_source_api'),
		'file' => _T('territoires_data:option_feed_type_source_file')
	];
	$valeurs['_type_source_defaut'] = 'api';

	// Etape 2
	// Initialisation des saisies relatives au mapping et à l'identification du feed
	// -- options de décodage : délimiteur si fichier CSV, racine à partir de laquelle paramétrer le mapping
	$valeurs['_label_decodage'] = _request('_label_decodage');
	$valeurs['_explication_decodage'] = _request('_explication_decodage');
	// -- explication du mapping dépendant du format de la source
	$valeurs['_explication_mapping_code'] = _request('_explication_mapping_code');
	$valeurs['_explication_mapping_valeur'] = _request('_explication_mapping_valeur');
	// -- Type de code de territoire utilisé dans le jeu de données
	$valeurs['_types_code'] = _request('_types_code');
	$valeurs['_type_code_defaut'] = _request('_type_code_defaut');
	// -- Format de la source et feed id par défaut
	$valeurs['_format_source'] = _request('_format_source');
	$valeurs['_feed_id_defaut'] = _request('_feed_id_defaut');

	// Récupération des saisies éventuelles de l'étape 2
	$valeurs['type_code'] = _request('type_code') ?? '';
	$valeurs['decodage'] = _request('decodage') ?? '';
	$valeurs['mapping_code'] = _request('mapping_code') ?? '';
	$valeurs['mapping_valeur'] = _request('mapping_valeur') ?? '';
	$valeurs['feed_id'] = _request('feed_id') ?? '';
	$valeurs['titre'] = _request('titre') ?? '';

	// Préciser le nombre d'étapes du formulaire
	$valeurs['_etapes'] = 2;

	return $valeurs;
}

/**
 * Identification des saisies Fichiers du formulaire :
 *
 * @param string      $id_categorie Identifiant de la catégorie des feeds. Permet de déterminer le type d'extra.
 * @param null|string $redirect     URL de retour suite à un traitement ok ou vide sinon. Si vie on renvoie sur l'admin
 *
 * @return array Tableau des variables des saisies Fichiers.
 */
function formulaires_creer_territoire_feed_fichiers(string $id_categorie, string $redirect = '') : array {
	return ['fichier_source'];
}

/**
 * Vérification du formulaire :.
 *
 * @param string      $id_categorie Identifiant de la catégorie des feeds. Permet de déterminer le type d'extra.
 * @param null|string $redirect     URL de retour suite à un traitement ok ou vide sinon. Si vide on renvoie sur l'admin
 *
 * @return array Message d'erreur si aucun pays choisi alors que la configuration du type de teritoire l'oblige.
 *               Sinon, chargement des champs utiles à l'étape 2 :
 */
function formulaires_creer_territoire_feed_verifier_1(string $id_categorie, string $redirect = '') : array {
	// Initialisation des erreurs de vérification.
	$erreurs = [];

	// On collecte les paramètres de l'étape 1 soit pour vérification pour pour élaborer d'autres variables pour l'étape 2.
	$type = _request('type');
	$variable_pays = 'pays_' . $type;
	$pays = _request($variable_pays) ?? '';
	$type_source = _request('type_source');
	$extra = _request('extra');

	// Vérification du choix d'un pays pour les types qui le requiert
	if (
		include_spip('inc/config')
		and lire_config("territoires/{$type}/populated_by_country", false)
		and !$pays
	) {
		$erreurs[$variable_pays] = _T('info_obligatoire');
	}

	// Vérification du choix d'un fichier valide ou d'une URL valide (on ne teste pas la validité du contenu
	if ($type_source === 'api') {
		$variable_source = _request('url_source');
		if (empty($variable_source)) {
			// Aucune URL saisie.
			$erreurs['url_source'] = _T('info_obligatoire');
		} elseif (
			include_spip('inc/distant')
			and (valider_url_distante($variable_source) === false)
		) {
			// URL distante non valide.
			// TODO : voir aussi filter_var(url, FILTER_VALIDATE_URL)
			$erreurs['url_source'] = _T('territoires_data:erreur_feed_url_source');
		}
	} else {
		$fichiers = _request('_fichiers');
		$variable_source = $fichiers['fichier_source'][0];
		if (empty($variable_source['name'])) {
			// Aucun fichier choisi.
			$erreurs['fichier_source'] = _T('info_obligatoire');
		} elseif (
			empty($variable_source['mime'])
			or (!in_array($variable_source['mime'], ['application/json', 'application/xml', 'text/csv']))
		) {
			// Format de fichier invalide
			$erreurs['fichier_source'] = _T('territoires_data:erreur_feed_format_fichier');
		}
	}

	// Constitution des variables et paramètres d'affichage de l'étape 2
	if (!$erreurs) {
		// -- Types de code possibles pour les territoires concernés
		$types_code['iso_territoire'] = _T('territoire:champ_iso_territoire_label') . ' (iso_territoire)';
		$where = [
			'type_extra=' . sql_quote('code'),
			'type=' . sql_quote($type),
			'iso_pays=' . sql_quote($pays),
		];
		$ids_code = sql_allfetsel('extra', 'spip_territoires_extras', $where, 'extra');
		if ($ids_code) {
			foreach (array_column($ids_code, 'extra') as $_code) {
				$types_code[$_code] = _T('territoire_extra:extra_' . $_code) . " ({$_code})";
			}
		}
		set_request('_types_code', $types_code);
		set_request('_type_code_defaut', 'iso_territoire');

		// -- Option de décodage de la source : pour une api on s'attend toujours à du JSON, pour un fichier à du JSON, XML ou CSV
		$format = 'json';
		if ($type_source === 'file') {
			// Extraire l'extension pour déterminer l'option de décodage à présenter
			[, $format] = explode('/', $variable_source['mime']);
		}
		set_request('_format_source', $format);
		if ($format === 'csv') {
			set_request('_label_decodage', _T('territoires_data:label_feed_decodage_delimiteur'));
			set_request('_explication_decodage', _T('territoires_data:explication_feed_decodage_delimiteur'));
			set_request('_explication_mapping_code', _T('territoires_data:explication_feed_mapping_code_csv'));
			set_request('_explication_mapping_valeur', _T('territoires_data:explication_feed_mapping_valeur_csv'));
		} else {
			set_request('_label_decodage', _T('territoires_data:label_feed_decodage_racine'));
			set_request('_explication_decodage', _T('territoires_data:explication_feed_decodage_racine'));
			set_request('_explication_mapping_code', _T('territoires_data:explication_feed_mapping_code'));
			set_request('_explication_mapping_valeur', _T('territoires_data:explication_feed_mapping_valeur'));
		}

		// -- Identifiant proposé pour le feed
		include_spip('inc/ezmashup_feed');
		$id_feed = "{$type}_" . ($pays ? "{$pays}_" : '') . $extra;
		$ressource = [
			'type' => 'config'
		];
		if (feed_ressource_existe('territoires_data', $id_feed, $ressource)) {
			// On rajoute un suffixe que l'utilisateur devra modifier
			$id_feed .= '_xxx';
		}
		set_request('_feed_id_defaut', strtolower($id_feed));
	}

	return $erreurs;
}

/**
 * Vérification du formulaire : détection des erreurs de saisie de l'étape 2.
 *
 * @param string      $id_categorie Identifiant de la catégorie des feeds. Permet de déterminer le type d'extra.
 * @param null|string $redirect     URL de retour suite à un traitement ok ou vide sinon. Si vide on renvoie sur l'admin
 *
 * @return array Message d'erreur ou vide sinon
 */
function formulaires_creer_territoire_feed_verifier_2(string $id_categorie, string $redirect = '') : array {
	// Initialisation des erreurs de vérification.
	$erreurs = [];

	// On collecte les paramètres nécessaires de l'étape 1.
	$format_source = _request('_format_source');
	$id_feed = _request('feed_id');

	// On collecte les paramètres de l'étape 2.
	$decodage = _request('decodage');
	$mapping_code = _request('mapping_code');
	$mapping_valeur = _request('mapping_valeur');

	// Vérification de la saisie des mappings qui ne doivent contenir qu'un ou plusieurs mots séparés par des '/'
	if ($format_source !== 'csv') {
		if (!preg_match('#^[\w/]+$#i', $mapping_code)) {
			$erreurs['mapping_code'] = _T('territoires_data:erreur_feed_mapping_code');
		}
		if (!preg_match('#^[\w/]+$#i', $mapping_valeur)) {
			$erreurs['mapping_valeur'] = _T('territoires_data:erreur_feed_mapping_valeur');
		}
	}

	// Vérification du décodage en fonction du type de source
	// -- si fichier csv : on attend un délimiteur parmi ',', ';', '|' et '\t' ou vide (la valeur par défaut est la virgule)
	// -- sinon : on attend un index comme pour le mapping ou vide
	if ($decodage) {
		if ($format_source === 'csv') {
			if (!in_array($decodage, [',', ';', "\t"])) {
				$erreurs['decodage'] = _T('territoires_data:erreur_feed_decodage');
			}
		} elseif (!preg_match('#^[\w/]+$#i', $decodage)) {
			$erreurs['decodage'] = _T('territoires_data:erreur_feed_mapping_code');
		}
	}

	// Vérification de la saisie du feed id
	include_spip('inc/ezmashup_feed');
	if (!preg_match('#^[\w]+$#i', $id_feed)) {
		$erreurs['feed_id'] = _T('territoires_data:erreur_id_invalide');
	} elseif (feed_ressource_existe('territoires_data', $id_feed, ['type' => 'config'])) {
		$erreurs['feed_id'] = _T('territoires_data:erreur_id_existe');
	}

	return $erreurs;
}

/**
 * Exécution du formulaire : .
 *
 * @param string      $id_categorie Identifiant de la catégorie des feeds. Permet de déterminer le type d'extra.
 * @param null|string $redirect     URL de retour suite à un traitement ok ou vide sinon. Si vide on renvoie sur l'admin
 *
 * @return array Tableau retourné par le formulaire contenant toujours un message de bonne exécution ou
 *               d'erreur.
 */
function formulaires_creer_territoire_feed_traiter(string $id_categorie, string $redirect = '') : array {
	// Initialisation du retour de la fonction
	$retour = [];

	// Récupération des variables constitutives du YAML
	// -- étape 1
	$type = _request('type');
	$pays = _request('pays_' . $type) ?? '';
	$type_source = _request('type_source');
	$format_source = _request('format_source');
	$extra = _request('extra');

	// -- étape 2
	include_spip('inc/charsets');
	$type_code = _request('type_code');
	$decodage = trim(_request('decodage')) ?? '';
	$mapping_code = translitteration(_request('mapping_code'));
	$mapping_valeur = translitteration(_request('mapping_valeur'));
	$id_feed = _request('feed_id');
	$titre_feed = _request('titre');

	// Déterminer la categorie et le type d'extra
	// -- la catégorie est toujours renvoyé dans le post du formulaire
	$categorie = _request('categorie');
	$type_extra = str_replace('territory_', '', $categorie);

	// Initialisation du YAML à partir du template
	include_spip('inc/yaml');
	$yaml_template = _DIR_PLUGIN_TERRITOIRES_DATA . 'ezmashup/territoires_data.config.yaml';
	$description_yaml = yaml_decode_file($yaml_template);

	// Personnalisation du YAML en fonction du contexte des saisies.
	// On considère que toutes les vérifications ont été faites et que donc les variables saisies sont cohérentes
	// -- identification
	$description_yaml['title'] = $titre_feed;
	// -- catégorisation
	$description_yaml['category'] = $categorie;
	// -- tags : on détermine le type d'extra à partir de la catégorie
	$description_yaml['tags']['type'] = $type;
	$description_yaml['tags']['pays'] = $pays;
	$description_yaml['tags']['_type_id'] = $type_code;
	$description_yaml['tags']['_type_extra'] = $type_extra;
	// -- mapping : basic, static et unused fields
	if ($type_code === 'iso_territoire') {
		// Le code primaire est utilisé dans jeu de données, il n'est pas utile de passer par un code alternatif
		// - le champ `code_feed` est donc inutile
		$description_yaml['mapping']['basic_fields']['iso_territoire'] = $mapping_code;
		unset($description_yaml['mapping']['basic_fields']['code_feed'], $description_yaml['mapping']['unused_fields']);
	} else {
		// Le jeu de données utilise un code alternatif au code primaire
		// - le champ `code_feed` est donc nécessaire temporairement
		$description_yaml['mapping']['basic_fields']['code_feed'] = $mapping_code;
	}
	$description_yaml['mapping']['basic_fields']['valeur'] = $mapping_valeur;
	$description_yaml['mapping']['static_fields']['extra'] = $extra;
	// -- la source : pour l'instant les crédits ne sont pas supportés, elle porte toujours l'id `source_1`
	//    L'uri sera remplie plus avant pour traiter le cas du fichier télécharger à copier dans l'emplacement final
	$id_source = 'basic_1';
	$description_yaml['sources_basic'][$id_source]['source']['type'] = $type_source;
	$description_yaml['sources_basic'][$id_source]['source']['format'] = $format_source;
	if ($format_source === 'csv') {
		unset($description_yaml['sources_basic'][$id_source]['decoding']['root_node']);
		$description_yaml['sources_basic'][$id_source]['decoding']['delimiter'] = $decodage;
	} else {
		unset($description_yaml['sources_basic'][$id_source]['decoding']['delimiter']);
		$description_yaml['sources_basic'][$id_source]['decoding']['root_node'] = $decodage;
	}
	// -- crédits
	$description_yaml['sources_basic'][$id_source]['source']['license'] = _request('credit_licence') ?? '';
	$description_yaml['sources_basic'][$id_source]['source']['version'] = _request('credit_version') ?? '';
	$description_yaml['sources_basic'][$id_source]['source']['last_update'] = _request('credit_date') ?? '';
	$description_yaml['sources_basic'][$id_source]['provider']['name'] = _request('fournisseur') ?? '';
	$description_yaml['sources_basic'][$id_source]['provider']['url'] = _request('fournisseur_url') ?? '';

	// Identification de la source et mise à jour dans la description YAML
	// -- si la source est un fichier, on le copie dans le dossier du feed
	$source = '';
	if ($type_source === 'api') {
		$source = _request('url_source');
	} else {
		// Récupération du fichier qui est stocké temporairement dans tmp/ par CVT Upload
		$fichiers = _request('_fichiers');
		$fichier = $fichiers['fichier_source'][0]['tmp_name'];

		// Lecture du fichier temporaire
		include_spip('inc/flock');
		lire_fichier($fichier, $contenu_source);

		// Stockage de la source dans son emplacement final
		$ressource = [
			'type'     => 'source',
			'format'   => $format_source,
			'decodage' => $decodage,
			'id'       => $id_source
		];
		if ($fichier_source = feed_ressource_ecrire('territoires_data', $id_feed, $ressource, $contenu_source)) {
			// -- extraire l'uri pour la description YAML
			$source = basename($fichier_source);
			// -- suppression du fichier temporaire
			@unlink($fichier);
		} else {
			$retour['message_erreur'] = _T('territoires_data:erreur_recuperation_source');
		}
	}

	if (empty($retour['message_erreur'])) {
		// On finalise la description du YAML avec la source
		$description_yaml['sources_basic'][$id_source]['source']['uri'] = $source;

		// Ecriture du YAML
		include_spip('inc/ezmashup_feed');
		$ressource = [
			'type' => 'config',
		];
		if (feed_ressource_ecrire('territoires_data', $id_feed, $ressource, $description_yaml)) {
			// Insertion du YAML en base de données
			feed_charger('territoires_data');
		} else {
			$retour['message_erreur'] = _T('territoires_data:erreur_ecriture_config');
		}
	}

	// Redirection vers la page des jeux de données si tout s'est bien passé
	if (empty($retour['message_erreur'])) {
		$retour['redirect'] = $redirect
			?: parametre_url(
				parametre_url(
					generer_url_ecrire(
						'peupler_data'
					),
					'composant',
					'jeu'
				),
				'category',
				$categorie
			);
	}

	return $retour;
}
