<?php
/**
 * Ce fichier contient les fonctions d'API du plugin Jeux de données pour Territoires utilisées comme filtre dans les squelettes.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_TERRITOIRES_DATA_REGEX_FORMAT')) {
	/**
	 * Expression régulière de contrôle des formats.
	 */
	define('_TERRITOIRES_DATA_REGEX_FORMAT', '/date|string|integer|float\d*/is');
}

if (!defined('_TERRITOIRES_DATA_OPTIONS_UNITE')) {
	/**
	 * Liste des options possibles sur le formatage de l'unité.
	 */
	define(
		'_TERRITOIRES_DATA_OPTIONS_UNITE',
		[
			'unite_id',
			'unite',
		]
	);
}

/**
 * Formate la valeur d'un extra en fonction de sa nature ou d'un format imposé.
 * La fonction peut aussi rajouter l'unité de l'extra.
 *
 * @api
 *
 * @param string     $extra   La nature de l'extra ou vide uniquement si on force le format dans les options.
 * @param mixed      $valeur  La valeur de l'extra.
 * @param null|array $options Options pour le formatage.
 *
 * @return string Valeur de l'extra formatée.
 */
function territoire_extra_formater_valeur(string $extra, mixed $valeur, ?array $options = []) : string {
	// Config des extras
	static $config = [];

	// Standardisation des options
	static $options_defaut = [
		// Options concernant la valeur
		'format'             => '',
		'separateur_decimal' => ',',
		'separateur_millier' => '&nbsp;',
		// Options concernant l'unité elle-même
		'afficher_unite'   => false,
		'separateur_unite' => '&nbsp;',
		'unite_id'         => '',
		'unite'            => '',
		// Options concernant la chaine produite
		'utf8' => false,
	];

	// La valeur est formatée à partir du format d'un extra ou en forçant le format
	// -- il est donc indispensable que l'un ou l'autre des paramètres soit présents et corrects, le format étant prioritaire
	// -- on standardise les options en ne retenant que celles utiles
	$options = array_merge($options_defaut, array_intersect_key($options, $options_defaut));

	// Identifier le format de la donnée
	// -- priorité au format fourni devant celui de l'extra
	$format = 'string';
	if ($options['format']) {
		$format = $options['format'];
	} elseif ($extra) {
		if (!$config) {
			include_spip('inc/config');
			$config = lire_config('territoires_data/extras', []);
		}
		$format = $config[$extra]['format'] ?? 'string';
	}

	// On renvoie par défaut la valeur d'entrée (si erreur ou string)
	$valeur_formatee = $valeur;

	// Formatage
	if ($format === 'date') {
		// Une date
		$valeur_formatee = affdate($valeur);
	} elseif ($format !== 'string') {
		// Un nombre
		// -- on détermine le nombre de décimales
		$decimales = $format === 'integer' ? 0 : (int) str_replace('float', '', $format);
		$valeur_formatee = number_format(
			(float) $valeur,
			$decimales,
			$options['separateur_decimal'],
			$options['separateur_millier']
		);

		// Ajout de l'unité si besoin :
		// -- si on passe le format et pas l'extra il est nécessaire de fournir l'unité ou son id
		// -- sinon on peut utiliser l'unité de l'extra, l'unité ou son id restant prioritaires
		if ($options['afficher_unite']) {
			// On ne passe pas l'unité en UTF8 même si demandé car on le fait une fois pour la chaine produite
			$options_unite = array_intersect_key($options, array_flip(_TERRITOIRES_DATA_OPTIONS_UNITE));
			$options_unite['utf8'] = false;
			$unite = territoire_extra_formater_unite(
				$options['extra'],
				$options_unite
			);
			$valeur_formatee .= $options['separateur_unite'] . $unite;
		}

		// Conversion en UTF-8 si besoin
		if ($options['utf8']) {
			$valeur_formatee = html_entity_decode(
				$valeur_formatee,
				ENT_NOQUOTES | ENT_SUBSTITUTE | ENT_HTML5,
				'UTF-8'
			);
		}
	}

	return $valeur_formatee;
}

/**
 * Formate l'unite d'un extra en fonction d'un certain nombre de paramètres.
 *
 * @api
 *
 * @param string     $extra   La nature de l'extra. Peut-être vide si on force l'unité dans les options.
 * @param null|array $options Options pour le formatage.
 *
 * @return string Chaine représentant l'unité
 */
function territoire_extra_formater_unite(string $extra, ?array $options = []) : string {
	// Standardisation des options
	$options_defaut = [
		// Options concernant l'unité elle-même
		'unite'    => '',
		'unite_id' => '',
		// Options concernant la chaine produite
		'utf8' => false,
	];

	// La valeur est formatée à partir du format d'un extra ou en forçant le format
	// -- il est donc indispensable que l'un ou l'autre des paramètres soit présents et corrects, le format étant prioritaire
	// -- on standardise les options en ne retenant que celles utiles
	$options = array_merge($options_defaut, array_intersect_key($options, $options_defaut));

	// Déterminer l'unité avec les priorités, si besoin :
	// -- si on passe le format et pas l'extra il est nécessaire de fournir l'unité ou son id
	// -- sinon on peut utiliser l'unité de l'extra, l'unité ou son id restant prioritaires
	$unite = '';
	if ($options['unite']) {
		$unite = $options['unite'];
	} elseif ($options['unite_id']) {
		$unite = territoire_unite_traduire($options['unite_id']);
	} elseif ($extra) {
		$unite = territoire_extra_traduire_unite($extra);
	}

	// Si on a besoin d'éviter le HTML (tooltip Chartjs par exemple) on supprime les tags et on passe en UTF-8
	if (
		$unite
		&& $options['utf8']
	) {
		$unite = html_entity_decode($unite, ENT_NOQUOTES | ENT_SUBSTITUTE | ENT_HTML5, 'UTF-8');
	}

	return $unite;
}

/**
 * @param string     $statistique
 * @param float|int  $valeur
 * @param null|array $options
 *
 * @return string
 */
function territoire_extra_formater_statistique(string $statistique, float|int $valeur, ?array $options = []) : string {
	// Standardisation des options
	$options_defaut = [
		// Options concernant le traitement des valeurs de la série
		'extra'              => '',
		'format'             => '',
		'separateur_decimal' => ',',
		'separateur_millier' => '&nbsp;',
		// Options concernant les chaines produites
		'utf8' => false,
	];

	// La valeur est formatée à partir du format d'un extra ou en forçant le format
	// -- il est donc indispensable que l'un ou l'autre des paramètres soit présents et corrects, le format étant prioritaire
	$options = array_merge($options_defaut, array_intersect_key($options, $options_defaut));

	// On formate en fonction de l'indicateur statistique
	if ($statistique === 'effectif') {
		// -- Entier : effectif
		$options['format'] = 'integer';
		$valeur_formatee = territoire_extra_formater_valeur('', $valeur, $options);
	} elseif (in_array($statistique, ['min', 'max', 'mediane', 'q1', 'q2', 'q3', 'etendue', 'iq'])) {
		// -- Format de l'extra : min, max, médiane, quartiles, étendue, interquartile
		$options['format'] = '';
		$valeur_formatee = territoire_extra_formater_valeur($options['extra'], $valeur, $options);
	} else {
		// -- Réel avec la précision fournie : le reste des statistiques
		$valeur_formatee = territoire_extra_formater_valeur('', $valeur, $options);
	}

	return $valeur_formatee;
}

/**
 * Formate les valeurs d'une série quantitative suivant les paramètres fournis.
 *
 * @param array      $serie
 * @param null|array $options
 *
 * @return array
 */
function territoire_extra_formater_serie(array $serie, ?array $options = []) : array {
	// Standardisation des options
	$options_defaut = [
		// Options concernant le traitement des valeurs de la série
		'extra'              => '',
		'format'             => '',
		'separateur_decimal' => ',',
		'separateur_millier' => '&nbsp;',
		// Options concernant l'unité elle-même
		'afficher_unite'   => false,
		'separateur_unite' => '&nbsp;',
		'unite_id'         => '',
		'unite'            => '',
		// Options concernant les chaines produites
		'utf8' => false,
	];

	// La série est formatée à partir du format d'un extra ou en forçant le format
	// -- il est donc indispensable que l'un ou l'autre des paramètres soit présents et corrects, le format étant prioritaire
	$options = array_merge($options_defaut, array_intersect_key($options, $options_defaut));
	$conversion_utf8 = $options['utf8'];

	// On vérifie les paramètres extra ou format car il est inutile de boucler sur la série sinon
	$serie_formatee = $serie;
	if (
		(
			include_spip('inc/config')
			&& lire_config("territoires_data/extras/{$options['extra']}", null)
		)
		or preg_match(_TERRITOIRES_DATA_REGEX_FORMAT, $options['format'])
	) {
		// Détermination de l'unité si besoin
		$unite = '';
		if ($options['afficher_unite']) {
			// On ne passe pas l'unité en UTF8 même si demandé car on le fait une fois pour la chaine produite
			$options_unite = array_intersect_key($options, array_flip(_TERRITOIRES_DATA_OPTIONS_UNITE));
			$options_unite['utf8'] = false;
			$unite = territoire_extra_formater_unite(
				$options['extra'],
				$options_unite
			);
			$unite = $options['separateur_unite'] . $unite;
		}

		// On supprime les options relatives à l'unité pour ne pas dupliquer les traitements et ne pas afficher l'unité
		// qui sera rajoutée dans la boucle ci-après
		// On désactive aussi la conversion utf8 qui sera faite si besoin sur la chaine produite complète
		$options = array_diff_key($options, array_flip(_TERRITOIRES_DATA_OPTIONS_UNITE));
		$options['afficher_unite'] = false;
		$options['utf8'] = false;

		// Formatage de la série
		foreach ($serie as $_territoire => $_valeur) {
			$serie_formatee[$_territoire] = territoire_extra_formater_valeur($options['extra'], $_valeur, $options) . $unite;
			if ($conversion_utf8) {
				$serie_formatee[$_territoire] = html_entity_decode(
					$serie_formatee[$_territoire],
					ENT_NOQUOTES | ENT_SUBSTITUTE | ENT_HTML5,
					'UTF-8'
				);
			}
		}
	}

	return $serie_formatee;
}

/**
 * Renvoie le libellé d'un extra.
 *
 * @api
 *
 * @param string $extra La nature de l'extra.
 *
 * @return string Libellé de l'extra.
 */
function territoire_extra_traduire_label(string $extra) : string {
	// On renvoie vide si erreur
	static $config = [];
	$label = '';

	if (!$config) {
		include_spip('inc/config');
		$config = lire_config('territoires_data/extras', []);
	}

	if (!empty($config[$extra]['label'])) {
		$label = typo($config[$extra]['label']);
	}

	return $label;
}

/**
 * Détermine l'unité d'un extra en fonction de sa nature.
 *
 * @api
 *
 * @param string $extra La nature de l'extra.
 *
 * @return string Unité de l'extra ou vide si aucune.
 */
function territoire_extra_traduire_unite(string $extra) : string {
	// On renvoie par défaut la valeur d'entrée
	static $config = [];
	$unite = '';

	if (!$config) {
		include_spip('inc/config');
		$config = lire_config('territoires_data/extras', []);
	}

	// Identifier l'unite de l'extra et en extraire le label
	if (!empty($config[$extra]['unite'])) {
		$unite_id = $config[$extra]['unite'];
		$unite = territoire_unite_traduire($unite_id);
	}

	return $unite;
}

/**
 * Traduit le format de la valeur de l'extra (chaine, nombre entier ou décimal, date).
 *
 * @api
 *
 * @param string $format Format de l'extra.
 *
 * @return string Libellé du format d'extra.
 */
function territoire_extra_traduire_format(string $format) : string {
	// On stocke les libellés de format pour le même hit
	static $formats = [];

	if (!isset($formats[$format])) {
		$decimales = strpos($format, 'float') !== false
			? (int) str_replace('float', '', $format)
			: 0;
		if ($decimales === 0) {
			$formats[$format] = _T('territoire_extra:format_extra_' . $format);
		} else {
			$formats[$format] = _T('territoire_extra:format_extra_float') . " ({$decimales})";
		}
	}

	return $formats[$format];
}

/**
 * Traduit le type d'extra (code, info ou stat).
 *
 * @api
 *
 * @param string $type Type de l'extra.
 *
 * @return string Libellé du type d'extra.
 */
function territoire_extra_traduire_type(string $type) : string {
	// On stocke les libellés de type pour le même hit
	static $types = [];

	if (!isset($types[$type])) {
		$types[$type] = $type ? _T('territoire_extra:type_extra_' . $type) : '';
	}

	return $types[$type];
}

/**
 * Traduit les méthodes de discrétisation d'une série issue d'un extra.
 *
 * @api
 *
 * @param string $methode Identifiant de la méthode de discrétisation
 *
 * @return string Libellé de la méthode de discrétisation d'un extra.
 */
function territoire_extra_traduire_discretisation(string $methode) : string {
	// On stocke les libellés de type pour le même hit
	static $methodes = [];

	if (!isset($methodes[$methode])) {
		$methodes[$methode] = $methode ? _T('territoire_extra:discretisation_' . $methode) : '';
	}

	return $methodes[$methode];
}

/**
 * Traduit les transformations d'une série issue d'un extra.
 *
 * @api
 *
 * @param string    $fonction Indentifiant de la fonction de transformation à appliquer à une série.
 * @param null|bool $abrege   Indique si on doit afficher le libellé complet ou abrégé (écriture mathématique).
 *                            Prend la valeur `false` par défaut
 *
 * @return string Libellé de la transformation d'un extra.
 */
function territoire_extra_traduire_transformation(string $fonction, ?bool $abrege = false) : string {
	// On stocke les libellés de type pour le même hit
	static $fonctions = [];

	if (!isset($fonctions[$fonction][$abrege])) {
		$fonctions[$fonction][$abrege] = '';
		if ($fonction) {
			$item = 'transformation_' . ($abrege ? 'abrege_' : '') . $fonction;
			$fonctions[$fonction][$abrege] = _T("territoire_extra:{$item}");
		}
	}

	return $fonctions[$fonction][$abrege];
}

/**
 * Traduit l'unité à partir de son identifiant.
 *
 * @api
 *
 * @param string $unite_id Identifiant de l'unité.
 *
 * @return string Libellé de l'unité.
 */
function territoire_unite_traduire(string $unite_id) : string {
	// On stocke les libellés d'unité pour le même hit
	static $config = [];
	static $unites = [];

	if (!$config) {
		include_spip('inc/config');
		$config = lire_config('territoires_data/unites', []);
	}

	if (!isset($unites[$unite_id])) {
		$unites[$unite_id] = '';
		if (!empty($config[$unite_id]['label'])) {
			$unites[$unite_id] = typo($config[$unite_id]['label']);
		}
	}

	return $unites[$unite_id];
}

/**
 * Compile la balise `#SERIE_STATISTIQUES` qui les principaux indicateurs statistiques d'une série quantitative d'un feed.
 *
 * La signature de la balise est : `#SERIE_STATISTIQUES{plugin, feed_id}`.
 *
 * @balise
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_SERIE_STATISTIQUES_dist(Champ $p) : Champ {
	// Récupération des arguments de la balise.
	// -- Identifiant du feed
	$plugin = interprete_argument_balise(1, $p);
	$plugin = isset($plugin) ? str_replace('\'', '"', $plugin) : '""';
	// -- Identifiant du feed
	$id_feed = interprete_argument_balise(2, $p);
	$id_feed = isset($id_feed) ? str_replace('\'', '"', $id_feed) : '""';

	// Calcul de la balise
	$p->code = "territoire_feed_compiler_statistiques({$plugin}, {$id_feed})";

	return $p;
}

/**
 * Calcul des statistiques de la série quantitative matérialisant le feed.
 * La fonction gère un cache JSON pour accélérer les traitements ainsi qu'une statique pour éviter plusieurs
 * lectures du cache dans le même hit.
 *
 * Le cache est créé ou recréé lors du peuplement du feed.
 *
 * @param string $plugin  Préfixe du plugin utilisateur
 * @param string $id_feed Identifiant du feed pour lequel on veut calculer les statistiques
 *
 * @return array Liste des indicateurs statistiques. L'index `variable` rappelle le contexte de la variable matérialisée
 *               par le feed.
 *
 * @throws Exception
 */
function territoire_feed_compiler_statistiques(string $plugin, string $id_feed) : array {
	// Initialisation en statique pour le feed
	static $statistiques = [];

	if (!isset($statistiques[$plugin][$id_feed])) {
		// On regarde si il existe un cache de statistiques pour le feed
		include_spip('inc/ezcache_cache');
		$cache = [
			'sous_dossier' => $plugin,
			'id_feed'      => $id_feed,
			'type'         => 'stats',
		];
		if (!$fichier_cache = cache_est_valide('territoires_data', 'analyse', $cache)) {
			// Il faut mettre à jour le cache avec les statistique de la série
			$statistiques[$plugin][$id_feed] = [];

			if ($serie = territoire_feed_lire_serie($plugin, $id_feed)) {
				// On acquiert les statistiques
				include_spip('inc/ezmath_statistique');
				$statistiques_feed = serie_statistiques($serie);

				// Ajout du contexte de la variable
				$statistiques_feed['variable'] = [
					'plugin'  => $plugin,
					'feed_id' => $id_feed,
					'extra'   => territoire_feed_lire_extra($plugin, $id_feed),
				];

				// Mise à jour du cache
				cache_ecrire('territoires_data', 'analyse', $cache, $statistiques_feed);

				// Stockage des statistiques du feed
				$statistiques[$plugin][$id_feed] = $statistiques_feed;
			}
		} else {
			// Lecture des données du fichier cache valide et stockage dans la statique
			$statistiques[$plugin][$id_feed] = cache_lire('territoires_data', 'analyse', $fichier_cache);
		}
	}

	return $statistiques[$plugin][$id_feed];
}

/**
 * Compile la balise `#INDICE_GEARY` qui renvoie l'indice geary d'un feed si il existe des centroides associés
 * aux territoires concernés par le feed.
 *
 * La signature de la balise est : `#INDICE_GEARY{plugin, feed_id}`.
 *
 * @balise
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_SERIE_INDICE_GEARY_dist(Champ $p) : Champ {
	// Récupération des arguments de la balise.
	// -- Identifiant du feed
	$plugin = interprete_argument_balise(1, $p);
	$plugin = isset($plugin) ? str_replace('\'', '"', $plugin) : '""';
	// -- Identifiant du feed
	$id_feed = interprete_argument_balise(2, $p);
	$id_feed = isset($id_feed) ? str_replace('\'', '"', $id_feed) : '""';

	// Calcul de la balise
	$p->code = "territoire_feed_calculer_geary({$plugin}, {$id_feed})";

	return $p;
}

/**
 * Calcul l'indice geary d'un feed si il existe des centroides associés aux territoires concernés par le feed.
 *
 * La fonction gère un cache JSON pour accélérer les traitements ainsi qu'une statique pour éviter plusieurs
 * lectures du cache dans le même hit. Si l'indice n'est pas calculable, le cache n'est pas créé.
 *
 * Le cache est créé à la demande ou lors du peuplement du feed ou de celui des centroides.
 *
 * @param string $plugin  Préfixe du plugin utilisateur
 * @param string $id_feed Identifiant du feed pour lequel on veut calculer les statistiques
 *
 * @return array Valeur de l'indice Geary ou null si erreur indexée par son id (`autocorrelation_geary`).
 *
 * @throws Exception
 */
function territoire_feed_calculer_geary(string $plugin, string $id_feed) : array {
	// Initialisation en statique pour le feed
	static $indices = [];

	if (!isset($indices[$plugin][$id_feed])) {
		// On regarde si il existe un cache de l'indice pour le feed
		include_spip('inc/ezcache_cache');
		$cache = [
			'sous_dossier' => $plugin,
			'id_feed'      => $id_feed,
			'type'         => 'geary',
		];
		if (!$fichier_cache = cache_est_valide('territoires_data', 'analyse', $cache)) {
			// Il faut mettre à jour le cache avec l'indice Geary de la série
			$indice = [];
			if (
				// Récupérer la série dans la table des extras de territoire et la matrice de pondération
				($serie = territoire_feed_lire_serie($plugin, $id_feed))
				&& ($ponderation = territoire_feed_calculer_ponderation($plugin, $id_feed))
			) {
				// -- calcul de l'indice
				include_spip('inc/ezmath_statistique');
				$indice['autocorrelation_geary'] = serie_autocorrelation_geary($serie, $ponderation);

				// Mise à jour du cache si l'indice a été calculé
				if (null !== $indice['autocorrelation_geary']) {
					cache_ecrire('territoires_data', 'analyse', $cache, $indice);
				}

				// Stockage des statistiques du feed dans tous les cas car si il est incalculable sur un hit, il
				// l'est pour tous
				$indices[$plugin][$id_feed] = $indice;
			}
		} else {
			// Lecture des données du fichier cache valide et stockage dans la statique
			$indices[$plugin][$id_feed] = cache_lire('territoires_data', 'analyse', $fichier_cache);
		}
	}

	return $indices[$plugin][$id_feed];
}

/**
 * Calcul de la matrice de pondération spatiale à partir des centroides associés aux territoires concernés par le feed.
 *
 * La fonction gère un cache JSON pour accélérer les traitements.
 * Si l'indice n'est pas calculable, le cache n'est pas créé.
 *
 * Le cache est créé à la demande ou lors du peuplement du feed ou de celui des centroides.
 *
 * @param string $plugin  Préfixe du plugin utilisateur
 * @param string $id_feed Identifiant du feed pour lequel on veut calculer les statistiques
 *
 * @return array Matrice de pondération spatiale ou vide si erreur.
 *
 * @throws Exception
 */
function territoire_feed_calculer_ponderation(string $plugin, string $id_feed) : array {
	// On regarde si il existe un cache de l'indice pour le feed
	include_spip('inc/ezcache_cache');
	$cache = [
		'sous_dossier' => $plugin,
		'id_feed'      => $id_feed,
		'type'         => 'mpond',
	];
	if (!$fichier_cache = cache_est_valide('territoires_data', 'analyse', $cache)) {
		// Initialisation de la matrice à vide
		$ponderation = [];

		// Il faut mettre à jour le cache avec la matrice de pondération spatiale de la série
		if (
			// Récupérer la série dans la table des extras de territoire et le feed
			include_spip('inc/ezmashup_feed')
			&& ($feed = feed_lire($plugin, $id_feed))
			&& ($serie = territoire_feed_lire_serie($plugin, $id_feed))
		) {
			// Calcul de l'indice de Geary (autocorrélation spatiale) sur la série
			// -- lecture des centroides des territoires si ils existent
			$type = $feed['tags']['type'] ?? '';
			$pays = $feed['tags']['pays'] ?? '';
			$select = ['iso_territoire', 'valeur', 'extra'];
			$where = [
				'type=' . sql_quote($type),
				'iso_pays=' . sql_quote($pays),
				sql_in('extra', ['latitude', 'longitude']),
			];
			$coordonnees = sql_allfetsel($select, 'spip_territoires_extras', $where);
			if ($coordonnees) {
				// -- constitution de la liste des points (latitude, longitude) correspondant aux territoires de la série
				$territoires = array_keys($serie);
				$points = [];
				foreach ($coordonnees as $_coordonnees) {
					if (in_array($_coordonnees['iso_territoire'], $territoires)) {
						if (!isset($points[$_coordonnees['iso_territoire']])) {
							$points[$_coordonnees['iso_territoire']] = [$_coordonnees['valeur']];
						} elseif ($_coordonnees['extra'] === 'latitude') {
							array_unshift($points[$_coordonnees['iso_territoire']], $_coordonnees['valeur']);
						} else {
							array_push($points[$_coordonnees['iso_territoire']], $_coordonnees['valeur']);
						}
					}
				}

				if (count($points) === count($serie)) {
					// -- détermination de la matrice de pondération spatiale à partir des centroides des territoires
					include_spip('inc/ezmath_geometrie');
					$ponderation = matrice_ponderation_spatiale($points);
				}
			}

			// Mise à jour du cache
			cache_ecrire('territoires_data', 'analyse', $cache, $ponderation);
		}
	} else {
		// Lecture des données du fichier cache valide et stockage dans la statique
		$ponderation = cache_lire('territoires_data', 'analyse', $fichier_cache);
	}

	return $ponderation;
}

/**
 * Compile la balise `#SERIE_CLINOGRAPHIQUE` qui renvoie le tableau des superficies cumulées en pourcentage indexé
 * par les valeurs ordonnées de la série.
 * La signature de la balise est : `#SERIE_CLINOGRAPHIQUE{plugin, feed_id}`.
 *
 * @balise
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_SERIE_CLINOGRAPHIQUE_dist(Champ $p) : Champ {
	// Récupération des arguments de la balise.
	// -- Identifiant du feed
	$plugin = interprete_argument_balise(1, $p);
	$plugin = isset($plugin) ? str_replace('\'', '"', $plugin) : '""';
	// -- Identifiant du feed
	$id_feed = interprete_argument_balise(2, $p);
	$id_feed = isset($id_feed) ? str_replace('\'', '"', $id_feed) : '""';

	// Calcul de la balise
	$p->code = "territoire_feed_calculer_courbe_clinographique({$plugin}, {$id_feed})";

	return $p;
}

/**
 * @param string $plugin
 * @param string $id_feed
 *
 * @return array
 * @throws Exception
 */
function territoire_feed_calculer_courbe_clinographique(string $plugin, string $id_feed) : array {
	// On regarde si il existe un cache de l'indice pour le feed
	include_spip('inc/ezcache_cache');
	$cache = [
		'sous_dossier' => $plugin,
		'id_feed'      => $id_feed,
		'type'         => 'clino',
	];
	if (!$fichier_cache = cache_est_valide('territoires_data', 'analyse', $cache)) {
		// Il faut mettre à jour le cache avec la courbe clinographique
		$courbe = [];

		if (
			// Récupérer la série dans la table des extras de territoire et le feed
			include_spip('inc/ezmashup_feed')
			&& ($feed = feed_lire($plugin, $id_feed))
			&& ($serie = territoire_feed_lire_serie($plugin, $id_feed))
		) {
			// On acquiert les superficies des territoires concernés
			// -- Les superficies si elle existe sont identifiées par un extra d'id `area`.
			//    On récupère celles du même couple (type, pays).
			//    Tous les territoires de la série doivent posséder une superficie sinon on renvoie un tableau vide
			$type = $feed['tags']['type'] ?? '';
			$pays = $feed['tags']['pays'] ?? '';

			$select = ['iso_territoire', 'valeur'];
			$where = [
				'type=' . sql_quote($type),
				'iso_pays=' . sql_quote($pays),
				'extra=' . sql_quote('area'),
			];
			$superficies = sql_allfetsel($select, 'spip_territoires_extras', $where);
			if ($superficies) {
				// On construit le tableau des superficies cummulées en pourcentage
				// -- créer le tableau des superficies indexé par les pays en extrayant seulement les territoires concernés
				$superficies = array_column($superficies, 'valeur', 'iso_territoire');
				$superficies = array_intersect_key($superficies, $serie);
				if (count($superficies) === count($serie)) {
					// -- Caster les superficies dans le format adéquat
					include_spip('inc/config');
					$format = lire_config('territoires_data/extras/area/format', '');
					include_spip('inc/ezmath_discretisation');
					$superficies = serie_transtyper($superficies, $format);

					// -- somme des superficies pour calculer les pourcentages
					$total_superficies = array_sum($superficies);

					// -- calcul des cumuls successifs
					$cumul_superficies = 0;
					asort($serie);
					foreach ($serie as $_iso => $_valeur) {
						// On met à jour le cumul en pourcentage
						$cumul_superficies += $superficies[$_iso];
						$courbe[] = [
							'variable'           => $_valeur,
							'superficie_cumulee' => round((100 * $cumul_superficies) / $total_superficies, 2),
							'superficie'         => $superficies[$_iso],
							'iso_territoire'     => $_iso
						];
					}
				}
			}
		}

		// Mise à jour du cache si la courbe a été calculée
		if ($courbe) {
			cache_ecrire('territoires_data', 'analyse', $cache, $courbe);
		}
	} else {
		// Lecture des données du fichier cache valide et stockage dans la statique
		$courbe = cache_lire('territoires_data', 'analyse', $fichier_cache);
	}

	return $courbe;
}

/**
 * Compile la balise `#SERIE_AIDE_DISCRETISATION` qui fournit les principaux indices aidant à la détermination
 * du nombre de classes.
 *
 * La signature de la balise est : `#SERIE_AIDE_DISCRETISATION{plugin, feed_id}`.
 *
 * @balise
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_SERIE_AIDE_DISCRETISATION_dist(Champ $p) : Champ {
	// Récupération des arguments de la balise.
	// -- Identifiant du feed
	$plugin = interprete_argument_balise(1, $p);
	$plugin = isset($plugin) ? str_replace('\'', '"', $plugin) : '""';
	// -- Identifiant du feed
	$id_feed = interprete_argument_balise(2, $p);
	$id_feed = isset($id_feed) ? str_replace('\'', '"', $id_feed) : '""';

	// Calcul de la balise
	$p->code = "territoire_feed_aider_discretisation({$plugin}, {$id_feed})";

	return $p;
}

/**
 * Calcul des indices permettant d'aider à la détermination du nombre de classes d'une dsicrétisation.
 * La fonction gère un cache JSON pour accélérer les traitements.
 *
 * Le cache est créé ou recréé lors du peuplement du feed.
 *
 * @param string $plugin  Préfixe du plugin utilisateur
 * @param string $id_feed Identifiant du feed pour lequel on veut calculer les statistiques
 *
 * @return array Liste des indices.
 *
 * @throws Exception
 */
function territoire_feed_aider_discretisation(string $plugin, string $id_feed) : array {
	// On regarde si il existe un cache de statistiques pour le feed
	include_spip('inc/ezcache_cache');
	$cache = [
		'sous_dossier' => $plugin,
		'id_feed'      => $id_feed,
		'type'         => 'nbcla',
	];
	if (!$fichier_cache = cache_est_valide('territoires_data', 'analyse', $cache)) {
		// Il faut mettre à jour le cache avec les indices de la série
		$indices = [];

		if ($serie = territoire_feed_lire_serie($plugin, $id_feed)) {
			// On calcule les indices un par un
			include_spip('inc/ezmath_discretisation');

			// Ajout des indices donnant le nombre de classe idéal
			$indices = [
				'huntsberger' => serie_indice_huntsberger($serie),
				'brooks'      => serie_indice_brooks($serie),
				'yule'        => serie_indice_yule($serie),
				'scott'       => serie_indice_scott($serie),
				'diaconis'    => serie_indice_diaconis($serie),
			];

			// Mise à jour du cache
			cache_ecrire('territoires_data', 'analyse', $cache, $indices);
		}
	} else {
		// Lecture des données du fichier cache valide et stockage dans la statique
		$indices = cache_lire('territoires_data', 'analyse', $fichier_cache);
	}

	return $indices;
}

/**
 * Compile la balise `#SERIE_DISCRETISATION` qui renvoie les classes et la série discrétisées.
 * La signature de la balise est : `#SERIE_DISCRETISATION{plugin, feed_id[, methode, nombre_classes, transformation]}`.
 *
 * @balise
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_SERIE_DISCRETISATION_dist(Champ $p) : Champ {
	// Récupération des arguments de la balise.
	// -- Identifiant du feed
	$plugin = interprete_argument_balise(1, $p);
	$plugin = isset($plugin) ? str_replace('\'', '"', $plugin) : '""';
	// -- Identifiant du feed
	$id_feed = interprete_argument_balise(2, $p);
	$id_feed = isset($id_feed) ? str_replace('\'', '"', $id_feed) : '""';
	// -- Méthode de discrétisation
	$methode = interprete_argument_balise(3, $p);
	$methode = isset($methode) ? str_replace('\'', '"', $methode) : '""';
	// -- Nombre de classes
	$nb_classes = interprete_argument_balise(4, $p);
	$nb_classes = $nb_classes ?? '5';
	// -- Transformation mathématique ou vide si on veut juste la série initiale
	$transformation = interprete_argument_balise(5, $p);
	$transformation = isset($transformation) ? str_replace('\'', '"', $transformation) : '""';

	// Calcul de la balise
	$p->code = "territoire_feed_discretiser_serie({$plugin}, {$id_feed}, {$methode}, {$nb_classes}, {$transformation})";

	return $p;
}

/**
 * @param string      $plugin            Préfixe du plugin utilisateur
 * @param string      $id_feed           Identifiant du feed pour lequel on veut calculer les statistiques
 * @param null|string $methode           Identifiant de la méthode de discrétisaiton
 * @param null|int    $nb_classes        Nombre de classes de la discrétisation
 * @param null|string $id_transformation Identifiant de la transformation mathématique à appliquer à la série ou vide
 *                                       sinon.
 *
 * @return array
 *
 * @throws Exception
 */
function territoire_feed_discretiser_serie(string $plugin, string $id_feed, ?string $methode = 'equivalence', ?int $nb_classes = 5, ?string $id_transformation = '') : array {
	// Initialisation pour le feed
	$discretisation = [];

	if (
		// Récupérer la série dans la table des extras de territoire et l'identifiant de l'extra
		($extra = territoire_feed_lire_extra($plugin, $id_feed))
		&& ($serie = territoire_feed_lire_serie($plugin, $id_feed))
	) {
		// Si la discrétisation se fait sur une série transformée, il faut remettre les bornes de classes
		// dans le référentiel de la série de base.
		$transformation = [];
		if ($id_transformation) {
			// Identification de la fonction réciproque à la transformation appliquée
			include_spip('inc/config');
			$transformation = lire_config("territoires_data/transformations/{$id_transformation}", []);
		}

		// On acquiert les classes et la série discrétisée
		include_spip('inc/ezmath_discretisation');
		$discretisation_feed = serie_discretisation($serie, $methode, $nb_classes, $transformation);
		if (!empty($discretisation_feed['classes'])) {
			$discretisation = $discretisation_feed;

			// Ajout des positions de la moyenne et de la médiane dans les classes
			include_spip('inc/ezmath_statistique');
			$discretisation['indicateurs'] = [
				'moyenne' => classe_trouver(serie_moyenne($serie), $discretisation_feed['classes']),
				'mediane' => classe_trouver(serie_mediane($serie), $discretisation_feed['classes']),
			];

			// Calcul des indices qualité de la discrétisation (TAI, I, Redondance R, Jenks D)
			$discretisation['qualite']['TAI'] = discretisation_indice_tai($serie, $discretisation_feed['classes']);
			$discretisation['qualite']['I'] = discretisation_indice_i($serie, $discretisation_feed['classes']);
			$discretisation['qualite']['R'] = discretisation_indice_redondance($serie, $discretisation_feed['classes']);
			$discretisation['qualite']['D'] = discretisation_indice_d($discretisation_feed['classes']);
			// Calcul de l'indice de qualité de Geary
			// -- Matrice de pondération
			$ponderation = territoire_feed_calculer_ponderation($plugin, $id_feed);
			$discretisation['qualite']['C'] = discretisation_indice_c($discretisation_feed['classes'], $ponderation);

			// Ajout du contexte de la variable
			$discretisation['variable'] = [
				'plugin'         => $plugin,
				'feed_id'        => $id_feed,
				'extra'          => $extra,
				'transformation' => $id_transformation,
			];
		}
	}

	return $discretisation;
}

/**
 * @param string      $plugin         Préfixe du plugin utilisateur
 * @param string      $id_feed        Identifiant du feed pour lequel on veut calculer les statistiques
 * @param null|string $transformation Identifiant de la transformation mathématique
 *
 * @return array Tableau associatif représentatif de la série, transformée ou pas.
 */
function territoire_feed_lire_serie(string $plugin, string $id_feed, ?string $transformation = '') : array {
	// Initialisation en statique de la config de l'extra et de la série de base (cad sans transformation)
	static $series = [];
	static $config = [];

	if (!$config) {
		include_spip('inc/config');
		$config = lire_config('territoires_data', []);
	}

	include_spip('inc/ezmath_discretisation');
	if (!isset($series[$plugin][$id_feed])) {
		$series[$plugin][$id_feed] = [];

		// Récupérer la série dans la table des extras de territoire
		$select = ['iso_territoire', 'extra', 'valeur'];
		$where = [
			'feed_id=' . sql_quote($id_feed),
		];
		$data = sql_allfetsel($select, 'spip_territoires_extras', $where);
		if ($data) {
			// On extrait la série sous la forme [id] = valeur
			$series[$plugin][$id_feed] = array_column($data, 'valeur', 'iso_territoire');

			// Suivant le format de l'extra, on cast ou pas les valeurs qui sont récupérées de la base comme des chaines
			$extra = $data[0]['extra'];
			$format = $config['extras'][$extra]['format'];
			$series[$plugin][$id_feed] = serie_transtyper($series[$plugin][$id_feed], $format);
		}
	}

	// Si une tranformation de la série du feed est demandée on l'applique
	if ($transformation) {
		$serie = serie_transformer($series[$plugin][$id_feed], $config['transformations'][$transformation]);
	} else {
		$serie = $series[$plugin][$id_feed];
	}

	return $serie;
}

/**
 * @todo il faudrait vérifier que le feed est chargé
 *
 * @param string $plugin
 * @param string $id_feed
 *
 * @return string
 */
function territoire_feed_lire_extra(string $plugin, string $id_feed) : string {
	// Initialisation en statique pour le feed
	static $extras = [];

	if (!isset($extras[$plugin][$id_feed])) {
		$extras[$plugin][$id_feed] = '';

		// Récupérer la série dans la table des extras de territoire
		$where = [
			'feed_id=' . sql_quote($id_feed),
		];
		$extra = sql_getfetsel('extra', 'spip_territoires_extras', $where);
		if ($extra) {
			$extras[$plugin][$id_feed] = $extra;
		}
	}

	return $extras[$plugin][$id_feed];
}

/**
 * @param string $asymetrie
 *
 * @return string
 */
function territoire_feed_qualifier_asymetrie(string $asymetrie) : string {
	// Interprétation de l'asymétrie (coefficient de Fisher)
	// -- on traduit la chaine affichée avec number_formet en français en float
	$asymetrie = (float) (str_replace(',', '.', $asymetrie));
	$asymetrie_absolu = abs($asymetrie);

	// -- Interprétation du signe
	if ($asymetrie > 0) {
		$item = 'info_asymetrie_gauche';
	} elseif ($asymetrie < 0) {
		$item = 'info_asymetrie_droite';
	} else {
		$item = '';
	}
	$type = $item ? _T('territoires_data:' . $item) : '';

	// Interprétation de la valeur absolue
	if ($asymetrie_absolu >= 2) {
		$item = 'info_asymetrie_forte';
	} elseif ($asymetrie_absolu >= 1) {
		$item = 'info_asymetrie_acceptable';
	} elseif ($asymetrie_absolu >= 0.5) {
		$item = 'info_asymetrie_legere';
	} else {
		$item = 'info_asymetrie_nulle';
	}
	$message = _T('territoires_data:' . $item, ['type' => $type]);

	return $message;
}

/**
 * @param string $kurtosis
 *
 * @return string
 */
function territoire_feed_qualifier_kurtosis(string $kurtosis) : string {
	// Interprétation de l'aplatissement (coefficient de Fisher)
	// -- on traduit la chaine affichée avec number_formet en français en float
	$kurtosis = (float) (str_replace(',', '.', $kurtosis));
	if ($kurtosis > 0) {
		$item = 'info_aplatissement_positif';
	} elseif ($kurtosis < 0) {
		$item = 'info_aplatissement_negatif';
	} else {
		$item = 'info_aplatissement_nul';
	}
	$message = _T('territoires_data:' . $item);

	return $message;
}

/**
 * @param string $cv
 *
 * @return string
 */
function territoire_feed_qualifier_cv(string $cv) : string {
	// Interprétation de l'aplatissement (coefficient de Fisher)
	// -- on traduit la chaine affichée avec number_formet en français en float
	$cv = (float) (str_replace(',', '.', $cv)) * 100;
	if ($cv > 40) {
		$item = 'info_cv_eleve';
	} elseif ($cv < 20) {
		$item = 'info_cv_faible';
	} else {
		$item = 'info_cv_modere';
	}
	$message = _T('territoires_data:' . $item);

	return $message;
}
