<?php
/**
 * Ce fichier contient les fonctions de service nécessitées par l'utilisation du plugin `Territoires`.
 *
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// ------------------------------------------------------
// ----------- SERVICES GENERAUX DU PLUGIN --------------
// ------------------------------------------------------

/**
 * Recharge la configuration du plugin.
 * Le service appelle un pipeline homonyme permettant de prolonger l'action au-delà du plugin utilisateur.
 *
 * @pipeline_appel configuration_recharger
 *
 * @param array $flux Description par défaut de la noisette.
 *
 * @return array Flux mis à jour
 */
function territoires_data_configuration_recharger(array $flux) : array {
	// Rechargement de la configuration statique.
	if ($flux['args']['plugin'] === 'territoires') {
		include_spip('territoires_data_administrations');
		// Acquisition de la configuration statique
		$config_statique = territoires_data_configurer();
		// Mise à jour en meta
		territoires_data_adapter_configuration($config_statique);
	}

	return $flux;
}
