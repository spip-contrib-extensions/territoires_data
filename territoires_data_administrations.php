<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_TERRITOIRES_DATA_CONFIG_EDITABLE')) {
	/**
	 * Liste des index de la config possédant des éléments éditables.
	 */
	define('_TERRITOIRES_DATA_CONFIG_EDITABLE', ['unites', 'extras', 'parametres']);
}

/**
 * Installation du schéma de données propre au plugin et gestion des migrations suivant
 * les évolutions du schéma.
 *
 * Le plugin rajoute une colonne feed_id dans la table `spip_territoires_extras` et crée une configuration des unites et
 * des extras fournit par défaut dans des feeds non éditables.
 *
 * @param string $nom_meta_base_version
 * @param string $version_cible
 *
 * @return void
 */
function territoires_data_upgrade($nom_meta_base_version, $version_cible) {
	// Initialisation de la configuration
	// -- Configuration statique du plugin Territoires
	$config_statique = territoires_data_configurer();

	// Ajout de la colonne feed_id et de la configuration statique
	$maj['create'] = [
		['sql_alter', "TABLE spip_territoires_extras ADD feed_id varchar(255) DEFAULT '' NOT NULL AFTER type_extra"],
		['ecrire_config', 'territoires_data', $config_statique]
	];

	// Ajout de la colonne discretisations
	$maj['2'] = [
		['sql_alter', "TABLE spip_feeds ADD discretisations TEXT DEFAULT '' NOT NULL AFTER is_editable"],
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);

	// Recharger les feeds si une modification de la configuration a eu lieu.
	include_spip('inc/ezmashup_feed');
	feed_charger('territoires_data', true);
}

/**
 * Suppression de l'ensemble du schéma de données propre au plugin.
 *
 * @param string $nom_meta_base_version
 *
 * @return void
 *
 * @throws Exception
 */
function territoires_data_vider_tables($nom_meta_base_version) {
	// Décharger les feeds du plugin : vider les tables, supprimer les consignations, vider le stockage des feeds
	include_spip('inc/ezmashup_feed');
	feed_decharger('territoires_data');

	// Supprimer les colonnes ajoutées dans la table des extras de territoires et dans celels des feeds
	sql_alter('TABLE spip_territoires_extras DROP feed_id');
	sql_alter('TABLE spip_feeds DROP discretisations');

	// Effacer les meta du plugin
	// -- configuration du plugin
	effacer_meta('territoires_data');
	// -- schéma du plugin
	effacer_meta($nom_meta_base_version);
}

/**
 * Fonction de mise à jour de la configuration du plugin en prenant en compte les ajouts utilisateurs et la nouvelle
 * configuration statique.
 *
 * @param array $config_statique Nouvelle configuration statique
 *
 * @return void
**/
function territoires_data_adapter_configuration(array $config_statique) : void {
	// Configuration actuelle : statique et utilisateur
	include_spip('inc/config');
	$config = lire_config('territoires_data', []);

	// Suppression des index de la config actuelle n'existant plus dans la config statique
	$index_statiques = array_keys($config_statique);
	foreach (array_keys($config) as $_index) {
		// Si l'index de la config courante est ni un index éditable ni un index statique, on le supprime
		if (
			!in_array($_index, _TERRITOIRES_DATA_CONFIG_EDITABLE)
			&& !in_array($_index, $index_statiques)
		) {
			unset($config[$_index]);
		}
	}

	// Mise à jour de la configuration statique
	foreach ($index_statiques as $_index_statique) {
		// On supprime la configuration statique de la configuration actuelle
		if (isset($config[$_index_statique])) {
			// L'index existe déjà dans la configuration actuelle : on va rafraichir ses éléments
			foreach ($config[$_index_statique] as $_cle => $_config) {
				if (
					!is_array($_config)
					|| (
						is_array($_config)
						&& empty($_config['is_editable'])
					)
				) {
					// L'élément est non éditable : on le supprime de la configuration, il sera mis à jour ci-après
					unset($config[$_index_statique][$_cle]);
				}
			}
		}

		// On ajoute la nouvelle configuration statique que l'index existe ou pas dans la configuration actuelle
		$config[$_index_statique] = array_merge(
			$config[$_index_statique] ?? [],
			$config_statique[$_index_statique]
		);
	}

	// Mise à jour en meta
	ecrire_config('territoires_data', $config);
}

/**
 * Renvoie la configuration statique (non éditable) du plugin.
 *
 * @api
 *
 * @return array Tableau de la configuration demandée.
 */
function territoires_data_configurer() : array {
	// Configuration des éléments de base du plugin (unites, extras, formats, types...)
	return [
		'unites' => [
			'km2' => [
				'label'       => '<:territoire_extra:unite_km2:>',
				'is_editable' => false
			],
			'hab' => [
				'label'       => '<:territoire_extra:unite_hab:>',
				'is_editable' => false
			],
		],
		'extras' => [
			'area' => [
				'label'       => '<:territoire_extra:extra_area:>',
				'is_editable' => false,
				'type_extra'  => 'info',
				'format'      => 'integer',
				'unite'       => 'km2'
			],
			'capital' => [
				'label'       => '<:territoire_extra:extra_capital:>',
				'is_editable' => false,
				'type_extra'  => 'info',
				'format'      => 'string',
				'unite'       => ''
			],
			'date_creation' => [
				'label'       => '<:territoire_extra:extra_date_creation:>',
				'is_editable' => false,
				'type_extra'  => 'info',
				'format'      => 'date',
				'unite'       => ''
			],
			'latitude' => [
				'label'       => '<:territoire_extra:extra_latitude:>',
				'is_editable' => false,
				'type_extra'  => 'info',
				'format'      => 'float2',
				'unite'       => ''
			],
			'longitude' => [
				'label'       => '<:territoire_extra:extra_longitude:>',
				'is_editable' => false,
				'type_extra'  => 'info',
				'format'      => 'float2',
				'unite'       => ''
			],
			'phone_id' => [
				'label'       => '<:territoire_extra:extra_phone_id:>',
				'is_editable' => false,
				'type_extra'  => 'info',
				'format'      => 'integer',
				'unite'       => ''
			],
			'tld' => [
				'label'       => '<:territoire_extra:extra_tld:>',
				'is_editable' => false,
				'type_extra'  => 'info',
				'format'      => 'string',
				'unite'       => ''
			],
			'population' => [
				'label'       => '<:territoire_extra:extra_population:>',
				'is_editable' => false,
				'type_extra'  => 'stat',
				'format'      => 'integer',
				'unite'       => 'hab'
			],
		],
		'formats' => [
			'integer',
			'float',
			'date',
			'string'
		],
		'types' => [
			'stat',
			'info'
		],
		'classes' => [
			'nb_liste'  => [4, 5, 6, 7, 8, 9],
			'nb_defaut' => 5
		],
		'discretisations' => [
			'equivalence',
			'progression_arithmetique',
			'progression_geometrique',
			'quantile',
			'standard',
			'standard_k',
			'standard_cote_z',
			'moyenne_emboitee',
			'jenks',
		],
		'transformations' => [
			'racine2' => [
				'fonction'   => 'sqrt',
				'parametre'  => null,
				'inverser'   => false,
			],
			'racine3' => [
				'fonction'   => 'pow',
				'parametre'  => 1 / 3,
				'inverser'   => false,
			],
			'puissance2' => [
				'fonction'   => 'pow',
				'parametre'  => 2,
				'inverser'   => false,
			],
			'puissance3' => [
				'fonction'   => 'pow',
				'parametre'  => 3,
				'inverser'   => false,
			],
			'logarithme' => [
				'fonction'   => 'log',
				'parametre'  => null,
				'inverser'   => false,
			],
			'logarithme10' => [
				'fonction'   => 'log10',
				'parametre'  => null,
				'inverser'   => false,
			],
			'inverse' => [
				'fonction'   => '',
				'parametre'  => null,
				'inverser'   => true,
			],
		]
	];
}
